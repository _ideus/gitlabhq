# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140814095920) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "allergens", force: true do |t|
    t.integer  "user_id"
    t.integer  "ingredient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "allergens", ["user_id", "ingredient_id"], name: "index_allergens_on_user_id_and_ingredient_id", unique: true, using: :btree

  create_table "brands", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id"
  end

  add_index "brands", ["company_id"], name: "index_brands_on_company_id", using: :btree
  add_index "brands", ["name"], name: "index_brands_on_name", unique: true, using: :btree

  create_table "broadcast_messages", force: true do |t|
    t.text     "message",    null: false
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer  "alert_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "color"
    t.string   "font"
  end

  create_table "certificate_symbols", force: true do |t|
    t.string   "abbreviation"
    t.string   "name"
    t.string   "english_name"
    t.text     "description"
    t.text     "uses"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", force: true do |t|
    t.string   "name",       null: false
    t.string   "address",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.string   "website"
    t.string   "pib"
  end

  create_table "deploy_keys_projects", force: true do |t|
    t.integer  "deploy_key_id", null: false
    t.integer  "project_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "deploy_keys_projects", ["project_id"], name: "index_deploy_keys_projects_on_project_id", using: :btree

  create_table "distribution_companies", force: true do |t|
    t.integer  "product_id"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emails", force: true do |t|
    t.integer  "user_id",    null: false
    t.string   "email",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "emails", ["email"], name: "index_emails_on_email", unique: true, using: :btree
  add_index "emails", ["user_id"], name: "index_emails_on_user_id", using: :btree

  create_table "events", force: true do |t|
    t.string   "target_type"
    t.integer  "target_id"
    t.string   "title"
    t.text     "data"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "action"
    t.integer  "author_id"
  end

  add_index "events", ["action"], name: "index_events_on_action", using: :btree
  add_index "events", ["author_id"], name: "index_events_on_author_id", using: :btree
  add_index "events", ["created_at"], name: "index_events_on_created_at", using: :btree
  add_index "events", ["project_id"], name: "index_events_on_project_id", using: :btree
  add_index "events", ["target_id"], name: "index_events_on_target_id", using: :btree
  add_index "events", ["target_type"], name: "index_events_on_target_type", using: :btree

  create_table "favorites", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favorites", ["product_id", "user_id"], name: "index_favorites_on_product_id_and_user_id", unique: true, using: :btree
  add_index "favorites", ["product_id"], name: "index_favorites_on_product_id", using: :btree
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id", using: :btree

  create_table "forked_project_links", force: true do |t|
    t.integer  "forked_to_project_id",   null: false
    t.integer  "forked_from_project_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "forked_project_links", ["forked_to_project_id"], name: "index_forked_project_links_on_forked_to_project_id", unique: true, using: :btree

  create_table "impressions", force: true do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "ingredients", force: true do |t|
    t.string   "name",             null: false
    t.string   "alternative_name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "issues", force: true do |t|
    t.string   "title"
    t.integer  "assignee_id"
    t.integer  "author_id"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",     default: 0
    t.string   "branch_name"
    t.text     "description"
    t.integer  "milestone_id"
    t.string   "state"
    t.integer  "iid"
  end

  add_index "issues", ["assignee_id"], name: "index_issues_on_assignee_id", using: :btree
  add_index "issues", ["author_id"], name: "index_issues_on_author_id", using: :btree
  add_index "issues", ["created_at"], name: "index_issues_on_created_at", using: :btree
  add_index "issues", ["milestone_id"], name: "index_issues_on_milestone_id", using: :btree
  add_index "issues", ["project_id", "iid"], name: "index_issues_on_project_id_and_iid", unique: true, using: :btree
  add_index "issues", ["project_id"], name: "index_issues_on_project_id", using: :btree
  add_index "issues", ["title"], name: "index_issues_on_title", using: :btree

  create_table "keys", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "key"
    t.string   "title"
    t.string   "type"
    t.string   "fingerprint"
  end

  add_index "keys", ["user_id"], name: "index_keys_on_user_id", using: :btree

  create_table "locations", force: true do |t|
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.string   "country"
    t.integer  "postal_code"
    t.string   "city"
    t.string   "address"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "manufacture_companies", force: true do |t|
    t.integer  "product_id"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "merge_request_diffs", force: true do |t|
    t.string   "state"
    t.text     "st_commits"
    t.text     "st_diffs"
    t.integer  "merge_request_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "merge_request_diffs", ["merge_request_id"], name: "index_merge_request_diffs_on_merge_request_id", unique: true, using: :btree

  create_table "merge_requests", force: true do |t|
    t.string   "target_branch",                 null: false
    t.string   "source_branch",                 null: false
    t.integer  "source_project_id",             null: false
    t.integer  "author_id"
    t.integer  "assignee_id"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "milestone_id"
    t.string   "state"
    t.string   "merge_status"
    t.integer  "target_project_id",             null: false
    t.integer  "iid"
    t.text     "description"
    t.integer  "position",          default: 0
  end

  add_index "merge_requests", ["assignee_id"], name: "index_merge_requests_on_assignee_id", using: :btree
  add_index "merge_requests", ["author_id"], name: "index_merge_requests_on_author_id", using: :btree
  add_index "merge_requests", ["created_at"], name: "index_merge_requests_on_created_at", using: :btree
  add_index "merge_requests", ["milestone_id"], name: "index_merge_requests_on_milestone_id", using: :btree
  add_index "merge_requests", ["source_branch"], name: "index_merge_requests_on_source_branch", using: :btree
  add_index "merge_requests", ["source_project_id"], name: "index_merge_requests_on_source_project_id", using: :btree
  add_index "merge_requests", ["target_branch"], name: "index_merge_requests_on_target_branch", using: :btree
  add_index "merge_requests", ["target_project_id", "iid"], name: "index_merge_requests_on_target_project_id_and_iid", unique: true, using: :btree
  add_index "merge_requests", ["title"], name: "index_merge_requests_on_title", using: :btree

  create_table "milestones", force: true do |t|
    t.string   "title",       null: false
    t.integer  "project_id",  null: false
    t.text     "description"
    t.date     "due_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state"
    t.integer  "iid"
  end

  add_index "milestones", ["due_date"], name: "index_milestones_on_due_date", using: :btree
  add_index "milestones", ["project_id", "iid"], name: "index_milestones_on_project_id_and_iid", unique: true, using: :btree
  add_index "milestones", ["project_id"], name: "index_milestones_on_project_id", using: :btree

  create_table "minerals", force: true do |t|
    t.string   "name",             null: false
    t.string   "alternative_name"
    t.string   "english_name"
    t.text     "description"
    t.float    "reference_value"
    t.string   "reference_unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "namespaces", force: true do |t|
    t.string   "name",                     null: false
    t.string   "path",                     null: false
    t.integer  "owner_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.string   "description", default: "", null: false
    t.string   "avatar"
  end

  add_index "namespaces", ["name"], name: "index_namespaces_on_name", using: :btree
  add_index "namespaces", ["owner_id"], name: "index_namespaces_on_owner_id", using: :btree
  add_index "namespaces", ["path"], name: "index_namespaces_on_path", using: :btree
  add_index "namespaces", ["type"], name: "index_namespaces_on_type", using: :btree

  create_table "notes", force: true do |t|
    t.text     "note"
    t.string   "noteable_type"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_id"
    t.string   "attachment"
    t.string   "line_code"
    t.string   "commit_id"
    t.integer  "noteable_id"
    t.boolean  "system",        default: false, null: false
    t.text     "st_diff"
  end

  add_index "notes", ["author_id"], name: "index_notes_on_author_id", using: :btree
  add_index "notes", ["commit_id"], name: "index_notes_on_commit_id", using: :btree
  add_index "notes", ["created_at"], name: "index_notes_on_created_at", using: :btree
  add_index "notes", ["noteable_id", "noteable_type"], name: "index_notes_on_noteable_id_and_noteable_type", using: :btree
  add_index "notes", ["noteable_type"], name: "index_notes_on_noteable_type", using: :btree
  add_index "notes", ["project_id", "noteable_type"], name: "index_notes_on_project_id_and_noteable_type", using: :btree
  add_index "notes", ["project_id"], name: "index_notes_on_project_id", using: :btree
  add_index "notes", ["updated_at"], name: "index_notes_on_updated_at", using: :btree

  create_table "nutrients", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.float    "reference_value"
    t.string   "reference_unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photos", force: true do |t|
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "imageable_id"
    t.string   "imageable_type"
  end

  create_table "product_allergens", force: true do |t|
    t.integer  "product_id"
    t.integer  "ingredient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_allergens", ["ingredient_id"], name: "index_product_allergens_on_ingredient_id", using: :btree
  add_index "product_allergens", ["product_id"], name: "index_product_allergens_on_product_id", using: :btree

  create_table "product_csymbols", force: true do |t|
    t.integer  "product_id"
    t.integer  "certificate_symbol_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_csymbols", ["product_id", "certificate_symbol_id"], name: "index_product_csymbols_on_product_id_and_certificate_symbol_id", unique: true, using: :btree
  add_index "product_csymbols", ["product_id"], name: "index_product_csymbols_on_product_id", using: :btree

  create_table "product_ingredients", force: true do |t|
    t.integer  "product_id"
    t.integer  "ingredient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_minerals", force: true do |t|
    t.integer  "product_id"
    t.integer  "mineral_id"
    t.float    "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_minerals", ["product_id", "mineral_id"], name: "index_product_minerals_on_product_id_and_mineral_id", unique: true, using: :btree
  add_index "product_minerals", ["product_id"], name: "index_product_minerals_on_product_id", using: :btree

  create_table "product_nutrients", force: true do |t|
    t.integer  "product_id"
    t.integer  "nutrient_id"
    t.float    "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_nutrients", ["product_id", "nutrient_id"], name: "index_product_nutrients_on_product_id_and_nutrient_id", unique: true, using: :btree
  add_index "product_nutrients", ["product_id"], name: "index_product_nutrients_on_product_id", using: :btree

  create_table "product_rsymbols", force: true do |t|
    t.integer  "product_id"
    t.integer  "recycling_symbol_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_rsymbols", ["product_id", "recycling_symbol_id"], name: "index_product_rsymbols_on_product_id_and_recycling_symbol_id", unique: true, using: :btree
  add_index "product_rsymbols", ["product_id"], name: "index_product_rsymbols_on_product_id", using: :btree

  create_table "product_vitamins", force: true do |t|
    t.integer  "product_id"
    t.integer  "vitamin_id"
    t.float    "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_vitamins", ["product_id", "vitamin_id"], name: "index_product_vitamins_on_product_id_and_vitamin_id", unique: true, using: :btree
  add_index "product_vitamins", ["product_id"], name: "index_product_vitamins_on_product_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "name",                                 null: false
    t.string   "description",                          null: false
    t.string   "barcode",                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "prepare_instructions"
    t.string   "storage_instructions"
    t.float    "net_weight"
    t.string   "reference_unit"
    t.string   "origin"
    t.integer  "brand_id"
    t.boolean  "c_prod",               default: false
  end

  create_table "projects", force: true do |t|
    t.string   "name"
    t.string   "path"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "creator_id"
    t.boolean  "issues_enabled",         default: true,     null: false
    t.boolean  "wall_enabled",           default: true,     null: false
    t.boolean  "merge_requests_enabled", default: true,     null: false
    t.boolean  "wiki_enabled",           default: true,     null: false
    t.integer  "namespace_id"
    t.string   "issues_tracker",         default: "gitlab", null: false
    t.string   "issues_tracker_id"
    t.boolean  "snippets_enabled",       default: true,     null: false
    t.datetime "last_activity_at"
    t.string   "import_url"
    t.integer  "visibility_level",       default: 0,        null: false
    t.boolean  "archived",               default: false,    null: false
    t.string   "import_status"
    t.float    "repository_size",        default: 0.0
  end

  add_index "projects", ["creator_id"], name: "index_projects_on_creator_id", using: :btree
  add_index "projects", ["last_activity_at"], name: "index_projects_on_last_activity_at", using: :btree
  add_index "projects", ["namespace_id"], name: "index_projects_on_namespace_id", using: :btree

  create_table "protected_branches", force: true do |t|
    t.integer  "project_id", null: false
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "protected_branches", ["project_id"], name: "index_protected_branches_on_project_id", using: :btree

  create_table "recycling_symbols", force: true do |t|
    t.integer  "recycling_code"
    t.string   "abbreviation"
    t.string   "name"
    t.string   "english_name"
    t.text     "description"
    t.text     "uses"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "services", force: true do |t|
    t.string   "type"
    t.string   "title"
    t.string   "token"
    t.integer  "project_id",                  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",      default: false, null: false
    t.string   "project_url"
    t.string   "subdomain"
    t.string   "room"
    t.text     "recipients"
    t.string   "api_key"
  end

  add_index "services", ["project_id"], name: "index_services_on_project_id", using: :btree

  create_table "snippets", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "author_id",                 null: false
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_name"
    t.datetime "expires_at"
    t.boolean  "private",    default: true, null: false
    t.string   "type"
  end

  add_index "snippets", ["author_id"], name: "index_snippets_on_author_id", using: :btree
  add_index "snippets", ["created_at"], name: "index_snippets_on_created_at", using: :btree
  add_index "snippets", ["expires_at"], name: "index_snippets_on_expires_at", using: :btree
  add_index "snippets", ["project_id"], name: "index_snippets_on_project_id", using: :btree

  create_table "stores", force: true do |t|
    t.string   "name"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context"
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string "name"
  end

  create_table "users", force: true do |t|
    t.string   "email",                    default: "",    null: false
    t.string   "encrypted_password",       default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.boolean  "admin",                    default: false, null: false
    t.integer  "projects_limit",           default: 10
    t.string   "skype",                    default: "",    null: false
    t.string   "linkedin",                 default: "",    null: false
    t.string   "twitter",                  default: "",    null: false
    t.string   "authentication_token"
    t.integer  "theme_id",                 default: 1,     null: false
    t.string   "bio"
    t.integer  "failed_attempts",          default: 0
    t.datetime "locked_at"
    t.string   "extern_uid"
    t.string   "provider"
    t.string   "username"
    t.boolean  "can_create_group",         default: true,  null: false
    t.boolean  "can_create_team",          default: true,  null: false
    t.string   "state"
    t.integer  "color_scheme_id",          default: 1,     null: false
    t.integer  "notification_level",       default: 1,     null: false
    t.datetime "password_expires_at"
    t.integer  "created_by_id"
    t.string   "avatar"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.boolean  "hide_no_ssh_key",          default: false
    t.string   "website_url",              default: "",    null: false
    t.datetime "last_credential_check_at"
  end

  add_index "users", ["admin"], name: "index_users_on_admin", using: :btree
  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["current_sign_in_at"], name: "index_users_on_current_sign_in_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["extern_uid", "provider"], name: "index_users_on_extern_uid_and_provider", unique: true, using: :btree
  add_index "users", ["name"], name: "index_users_on_name", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", using: :btree

  create_table "users_groups", force: true do |t|
    t.integer  "group_access",                   null: false
    t.integer  "group_id",                       null: false
    t.integer  "user_id",                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "notification_level", default: 3, null: false
  end

  add_index "users_groups", ["user_id"], name: "index_users_groups_on_user_id", using: :btree

  create_table "users_projects", force: true do |t|
    t.integer  "user_id",                        null: false
    t.integer  "project_id",                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_access",     default: 0, null: false
    t.integer  "notification_level", default: 3, null: false
  end

  add_index "users_projects", ["project_access"], name: "index_users_projects_on_project_access", using: :btree
  add_index "users_projects", ["project_id"], name: "index_users_projects_on_project_id", using: :btree
  add_index "users_projects", ["user_id"], name: "index_users_projects_on_user_id", using: :btree

  create_table "vitamins", force: true do |t|
    t.string   "name",             null: false
    t.string   "alternative_name"
    t.string   "english_name"
    t.text     "description"
    t.float    "reference_value"
    t.string   "reference_unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "web_hooks", force: true do |t|
    t.string   "url"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type",                  default: "ProjectHook"
    t.integer  "service_id"
    t.boolean  "push_events",           default: true,          null: false
    t.boolean  "issues_events",         default: false,         null: false
    t.boolean  "merge_requests_events", default: false,         null: false
    t.boolean  "tag_push_events",       default: false
  end

  add_index "web_hooks", ["project_id"], name: "index_web_hooks_on_project_id", using: :btree

end
