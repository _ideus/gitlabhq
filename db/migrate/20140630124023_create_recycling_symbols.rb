class CreateRecyclingSymbols < ActiveRecord::Migration
  def change
    create_table :recycling_symbols do |t|
      t.integer :recycling_code
      t.string :abbreviation
      t.string :name
      t.string :english_name
      t.text :description
      t.text :uses

      t.timestamps
    end
  end
end
