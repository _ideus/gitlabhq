class CreateCertificateSymbols < ActiveRecord::Migration
  def change
    create_table :certificate_symbols do |t|
      t.integer :certificate_code
      t.string :abbreviation
      t.string :name
      t.string :english_name
      t.text :description
      t.text :uses

      t.timestamps
    end
  end
end
