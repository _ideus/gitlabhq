class AddDetailsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :prepare_instructions, :string
    add_column :products, :storage_instructions, :string
  end
end
