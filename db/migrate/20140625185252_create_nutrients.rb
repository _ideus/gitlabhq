class CreateNutrients < ActiveRecord::Migration
  def change
    create_table :nutrients do |t|
      t.string :name
      t.string :description
      t.float :reference_value
      t.string :reference_unit

      t.timestamps
    end
  end
end
