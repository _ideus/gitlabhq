class AddCompanyRefToBrands < ActiveRecord::Migration
  def change
    add_reference :brands, :company, index: true
  end
end
