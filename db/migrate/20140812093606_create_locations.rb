class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.integer :addressable_id
      t.string :addressable_type
      t.string :country
      t.integer :postal_code
      t.string :city
      t.string :address
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
