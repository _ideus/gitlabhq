class RemoveCertificateCodeFromCertificateSymbol < ActiveRecord::Migration
  def change
    remove_column :certificate_symbols, :certificate_code, :integer
  end
end
