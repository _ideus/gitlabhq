class CreateProductCsymbols < ActiveRecord::Migration
  def change
    create_table :product_csymbols do |t|
      t.integer :product_id
      t.integer :certificate_symbol_id

      t.timestamps
    end

    add_index :product_csymbols, [:product_id, :certificate_symbol_id], unique: true
    add_index :product_csymbols, :product_id
  end
end

