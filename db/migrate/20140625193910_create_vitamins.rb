class CreateVitamins < ActiveRecord::Migration
  def change
    create_table :vitamins do |t|
      t.string :name, null: false
      t.string :alternative_name
      t.string :english_name
      t.string :description
      t.float :reference_value
      t.string :reference_unit

      t.timestamps
    end
  end
end
