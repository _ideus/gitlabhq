class CreateProductNutrients < ActiveRecord::Migration
  def change
    create_table :product_nutrients do |t|
      t.integer :product_id
      t.integer :nutrient_id
      t.float :amount

      t.timestamps
    end

    add_index :product_nutrients, [:product_id, :nutrient_id], unique: true
    add_index :product_nutrients, :product_id
  end
end
