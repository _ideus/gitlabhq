class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :product, index: true
      t.references :user, index: true

      t.timestamps
    end

    add_index :favorites, [:product_id, :user_id], unique: true
  end
end
