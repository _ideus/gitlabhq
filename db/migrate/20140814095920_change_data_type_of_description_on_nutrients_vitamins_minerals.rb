class ChangeDataTypeOfDescriptionOnNutrientsVitaminsMinerals < ActiveRecord::Migration
  def change
    change_column :nutrients, :description, :text
    change_column :minerals, :description, :text
    change_column :vitamins, :description, :text
  end
end
