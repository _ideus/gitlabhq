class CreateProductMinerals < ActiveRecord::Migration
  def change
    create_table :product_minerals do |t|
      t.integer :product_id
      t.integer :mineral_id
      t.float :amount

      t.timestamps
    end

    add_index :product_minerals, [:product_id, :mineral_id], unique: true
    add_index :product_minerals, :product_id
  end
end
