class ChangeDescriptionTypeInIngredients < ActiveRecord::Migration
  def change
    change_column :ingredients, :description, :text
  end
end
