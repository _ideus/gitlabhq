class AddNewCompanyAttributesToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :phone, :string
    add_column :companies, :fax, :string
    add_column :companies, :email, :string
    add_column :companies, :website, :string
    add_column :companies, :pib, :string
  end
end
