class CreateDistributionCompanies < ActiveRecord::Migration
  def change
    create_table :distribution_companies do |t|
      t.integer :product_id
      t.integer :company_id

      t.timestamps
    end
  end
end
