class ChangePhotosColumns < ActiveRecord::Migration
  def change
    remove_column :photos, :product_id, :integer
    add_column :photos, :imageable_id, :integer
    add_column :photos, :imageable_type, :string
  end
end
