class AddBarcodeIndexToProducts < ActiveRecord::Migration
  def change
    add_index :products, :barcode, unique: true
  end
end