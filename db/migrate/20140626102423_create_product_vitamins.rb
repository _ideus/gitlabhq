class CreateProductVitamins < ActiveRecord::Migration
  def change
    create_table :product_vitamins do |t|
      t.integer :product_id
      t.integer :vitamin_id
      t.float :amount

      t.timestamps
    end

    add_index :product_vitamins, [:product_id, :vitamin_id], unique: true
    add_index :product_vitamins, :product_id
  end
end
