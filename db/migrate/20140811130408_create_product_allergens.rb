class CreateProductAllergens < ActiveRecord::Migration
  def change
    create_table :product_allergens do |t|
      t.references :product, index: true
      t.references :ingredient, index: true

      t.timestamps
    end
  end
end
