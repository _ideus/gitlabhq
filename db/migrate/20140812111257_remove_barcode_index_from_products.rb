class RemoveBarcodeIndexFromProducts < ActiveRecord::Migration
  def change
    remove_index :products, :barcode
  end
end
