class AddNetWeightToProducts < ActiveRecord::Migration
  def change
    add_column :products, :net_weight, :float
    add_column :products, :reference_unit, :string
  end
end
