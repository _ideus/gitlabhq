class AddIndexToAllergen < ActiveRecord::Migration
  def change
    add_index "allergens", ["user_id", "ingredient_id"], unique: true
  end
end
