class AddCprodToProduct < ActiveRecord::Migration
  def change
    add_column :products, :c_prod, :boolean, default: false
  end
end