class CreateProductRsymbols < ActiveRecord::Migration
  def change
    create_table :product_rsymbols do |t|
      t.integer :product_id
      t.integer :recycling_symbol_id

      t.timestamps
    end

    add_index :product_rsymbols, [:product_id, :recycling_symbol_id], unique: true
    add_index :product_rsymbols, :product_id
  end
end
