# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :recycling_symbol do
    recycling_code 1
    abbreviation "MyString"
    name "MyString"
    english_name "MyString"
    description "MyText"
    uses "MyText"
  end
end
