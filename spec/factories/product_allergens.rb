# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_allergen do
    product nil
    ingredient nil
  end
end
