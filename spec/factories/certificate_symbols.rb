# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :certificate_symbol do
    certificate_code 1
    abbreviation "MyString"
    name "MyString"
    english_name "MyString"
    description "MyText"
    uses "MyText"
  end
end
