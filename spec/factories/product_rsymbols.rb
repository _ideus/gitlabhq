# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_rsymbol do
    product_id 1
    recycling_symbol_id 1
  end
end
