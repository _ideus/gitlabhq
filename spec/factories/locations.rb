# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    addressable_id 1
    addressable_type "MyString"
    country "MyString"
    postal_code 1
    city "MyString"
    address "MyString"
    latitude 1.5
    longitude 1.5
  end
end
