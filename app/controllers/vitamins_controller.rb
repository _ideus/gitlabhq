class VitaminsController < ApplicationController
  before_filter :vitamin, except: [:index, :new, :create]

  def index
    @vitamins = Vitamin.all
    @vitamins = @vitamins.page(params[:page])
  end

  def show
  end

  def new
    @vitamin = Vitamin.new
  end

  def create
    @vitamin = Vitamin.new(vitamin_params, :without_protection => true)

    if @vitamin.save
      redirect_to vitamin_path(@vitamin)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @vitamin.update_attributes(vitamin_params, :without_protection => true)
      redirect_to vitamin_path(@vitamin)
    else
      render :edit
    end
  end

  def destroy
    @vitamin.destroy
    redirect_to vitamins_path
  end

  private

  def vitamin
    @vitamin = Vitamin.find(params[:id])
  end

  def vitamin_params
    params.require(:vitamin).permit(:name, :alternative_name, :english_name, :description)
  end

end