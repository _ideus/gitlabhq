class RecyclingSymbolsController < ApplicationController
  before_filter :recycling_symbol, except: [:index, :new, :create]

  def index
    @recycling_symbols = RecyclingSymbol.all
    @recycling_symbols = @recycling_symbols.page(params[:page])
  end

  def show
  end

  def new
    @recycling_symbol = RecyclingSymbol.new
    @recycling_symbol.photos.build
  end

  def create
    @recycling_symbol = RecyclingSymbol.new(symbol_params, :without_protection => true)

    if @recycling_symbol.save
      redirect_to recycling_symbol_path(@recycling_symbol)
    else
      render :new
    end
  end

  def edit
    if !@recycling_symbol.photos.first
      @recycling_symbol.photos.build
    end
  end

  def update
    if @recycling_symbol.update_attributes(symbol_params, :without_protection => true)
      redirect_to recycling_symbol_path(@recycling_symbol)
    else
      render :edit
    end
  end

  def destroy
    @recycling_symbol.destroy
    redirect_to recycling_symbols_path
  end

  private

  def recycling_symbol
    @recycling_symbol = RecyclingSymbol.find(params[:id])
  end

  def symbol_params
    params.require(:recycling_symbol).permit(:recycling_code, :abbreviation, :name, :english_name, :description, :uses, photos_attributes: [:id, :image])
  end
end