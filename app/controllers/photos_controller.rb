class PhotosController < ApplicationController

  def destroy_product
    @product = Product.find(params[:id])
    @product.photos.first.remove_image!
    @product.photos.first.destroy
    redirect_to edit_product_path(@product)
  end

  def destroy_company
    @company = Company.find(params[:id])
    @company.photos.first.remove_image!
    @company.photos.first.destroy
    redirect_to edit_company_path(@company)
  end

  def destroy_brand
    @brand = Brand.find(params[:id])
    @brand.photos.first.remove_image!
    @brand.photos.first.destroy
    redirect_to edit_brand_path(@brand)
  end

  def destroy_recycling_symbol
    @recycling_symbol = RecyclingSymbol.find(params[:id])
    @recycling_symbol.photos.first.remove_image!
    @recycling_symbol.photos.first.destroy
    redirect_to edit_recycling_symbol_path(@recycling_symbol)
  end

  def destroy_certificate_symbol
    @certificate_symbol = CertificateSymbol.find(params[:id])
    @certificate_symbol.photos.first.remove_image!
    @certificate_symbol.photos.first.destroy
    redirect_to edit_certificate_symbol_path(@certificate_symbol)
  end
end