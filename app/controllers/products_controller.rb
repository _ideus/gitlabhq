class ProductsController < ApplicationController
  before_filter :product, except: [:index, :new, :create, :tags, :list_ingredients, :existing_tags, :search]

  def index
    if params[:tag]
      @products = Product.tagged_with(params[:tag])
    else
      @products = Product.all
    end
    @products = @products.page(params[:page])
  end

  def search
    options = {
      t: params[:t],
      m: params[:m],
      d: params[:d]
    }
    response = Product.search(params[:q], options).page(params[:page])
    @products = response.results
    @records = response.records

    respond_to do |format|
      format.html
      format.json { render :json => @products.map { |r| {"name" => r.name, "id" => r.id} } }
    end
  end

  def show
    @product = Product.find(params[:id])
    @vitamins = @product.product_vitamins
    @ingredients = @product.product_ingredients
    @nutrients = @product.product_nutrients
    @minerals = @product.product_minerals

    if session["has_counted_view_#{params[:id]}"] == nil
      impressionist(@product)
      session["has_counted_view_#{params[:id]}"] = true
    end
  end

  def new
    @product = Product.new
    @product.photos.build
    initialize_nutrients
    initialize_vitamins
    initialize_minerals
    initialize_manufacture_company
    initialize_distribution_company

    initialize_product_rsymbols
    initialize_product_csymbols
  end

  def create
    @product = Product.new(product_params, :without_protection => true)

    if @product.save  
      redirect_to @product
    else
      initialize_product_rsymbols
      initialize_product_csymbols
      render :new
    end
  end

  def edit
    @product.photos.build if @product.photos.empty?
    @product.manufacture_companies.build if @product.manufacture_companies.empty?
    @product.distribution_companies.build if @product.distribution_companies.empty?

    initialize_product_rsymbols
    initialize_product_csymbols
    initialize_nutrients
    initialize_minerals
    initialize_vitamins
  end

  def update
    @product.c_prod = false
    if @product.update_attributes(product_params, :without_protection => true)
      redirect_to @product
    else
      @product.c_prod = true
      initialize_product_rsymbols
      initialize_product_csymbols
      render :edit
    end
  end

  def copy
    @existing_product = Product.find(params[:id])
    @product = @existing_product.dup
    @product.c_prod = true
    @product.barcode = ""
    @product.save

    @existing_product.tags.each do |tag|
      ActsAsTaggableOn::Tagging.create(taggable_id: @product.id, tag_id: tag.id, taggable_type: "Product", context: "tags")
    end

    @existing_product.manufacture_companies.each do |manufacture_company|
      ManufactureCompany.create({product_id: @product.id, company_id: manufacture_company.company_id}, :without_protection => true)
    end

    @existing_product.distribution_companies.each do |distribution_company|
      DistributionCompany.create({product_id: @product.id, company_id: distribution_company.company_id}, :without_protection => true)
    end

    @existing_product.product_vitamins.each do |vitamin|
      ProductVitamin.create({product_id: @product.id, vitamin_id: vitamin.vitamin_id, amount: vitamin.amount}, :without_protection => true)
    end

    @existing_product.product_nutrients.each do |nutrient|
      ProductNutrient.create({product_id: @product.id, nutrient_id: nutrient.nutrient_id, amount: nutrient.amount}, :without_protection => true)
    end

    @existing_product.product_minerals.each do |mineral|
      ProductMineral.create({product_id: @product.id, mineral_id: mineral.mineral_id, amount: mineral.amount}, :without_protection => true)
    end

    # check out
    @existing_product.ingredients.each do |ingredient|
      ProductIngredient.create({product_id: @product.id, ingredient_id: ingredient.id}, :without_protection => true)
    end

    @existing_product.product_rsymbols.each do |rsymbol|
      ProductRsymbol.create({product_id: @product.id, recycling_symbol_id: rsymbol.recycling_symbol_id}, :without_protection => true)
    end

    @existing_product.product_csymbols.each do |csymbol|
      ProductCsymbol.create({product_id: @product.id, certificate_symbol_id: csymbol.certificate_symbol_id}, :without_protection => true)
    end

    @product.brand = @existing_product.brand

    @product.photos.build if @product.photos.empty?
    @product.manufacture_companies.build if @product.manufacture_companies.empty?
    @product.distribution_companies.build if @product.distribution_companies.empty?

    redirect_to edit_product_path(@product)
  end

  def destroy
    @product.destroy
    redirect_to products_path
  end

  def tags
    @list_tags = ActsAsTaggableOn::Tag.select("id, name").where("name LIKE ?", "#{params[:q]}%").order(:name)
    respond_to do |format|
      format.json  { render json: @list_tags, :only => [:id, :name]}
    end
  end

  def favorite
    @favorite = @product.favorites.create({user: current_user}, without_protection: true)
    respond_to do |format|
      format.html { redirect_to @product}
      format.js
    end
  end

  def remove_favorite
    @favorite = Favorite.find_by(user_id: current_user.id, product_id: @product.id)
    @favorite.destroy if @favorite
    respond_to do |format|
      format.html { redirect_to @product}
      format.js
    end
  end

  def list_ingredients
    @product = Product.find(params[:id])
    arr = @product.ingredient_ids
    @ingredients = Ingredient.where("id in (?)", arr)
    respond_to do |format|
      format.json  { render :json => @ingredients}
    end
  end

  def existing_tags
    @product = Product.find(params[:id])
    @tags = @product.tags
    respond_to do |format|
      format.json  { render :json => @tags}
    end
  end

  private

  def product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :description, :barcode, :ac_ingredient_ids, :allergen_ingredient_ids, :brand_name, :net_weight, :reference_unit, :origin,
                                                          :prepare_instructions, :storage_instructions, :tag_list, :rsymbols, :csymbols,
                                                          product_ingredients_attributes: [:id, :ingredient_id, :product_id],
                                                          photos_attributes: [:id, :image],
                                                          product_nutrients_attributes: [:id, :nutrient_id, :product_id, :amount, :_destroy],
                                                          product_vitamins_attributes: [:id, :vitamin_id, :product_id, :amount, :_destroy],
                                                          product_minerals_attributes: [:id, :mineral_id, :product_id, :amount, :_destroy],
                                                          manufacture_companies_attributes: [:id, :company_id, :company_name, :product_id, :_destroy],
                                                          distribution_companies_attributes: [:id, :company_id, :company_name, :product_id, :_destroy],
                                                          favorites_attributes: [:id, :product_id, :user_id]
                                                          )
  end

  def initialize_nutrients
    Nutrient.all.each do |nutrient|
      if !@product.product_nutrients.exists?
        @product.product_nutrients.build({nutrient_id: nutrient.id}, without_protection: true)
      end
    end
  end

  def initialize_vitamins
    Vitamin.all.each do |vitamin|
      if !@product.product_vitamins.exists?
        @product.product_vitamins.build({vitamin_id: vitamin.id}, without_protection: true)
      end
    end
  end

  def initialize_minerals
    Mineral.all.each do |mineral|
      if !@product.product_minerals.exists?
        @product.product_minerals.build({mineral_id: mineral.id}, without_protection: true)
      end
    end
  end

  def initialize_manufacture_company
    @product.manufacture_companies.build
  end

  def initialize_distribution_company
    @product.distribution_companies.build
  end

  def initialize_product_rsymbols
    @recycling_symbols = RecyclingSymbol.all
  end

  def initialize_product_csymbols
    @certificate_symbols = CertificateSymbol.all
  end
end