class CertificateSymbolsController < ApplicationController
  before_filter :certificate_symbol, except: [:index, :new, :create]

  def index
    @certificate_symbols = CertificateSymbol.all
    @certificate_symbols = @certificate_symbols.page(params[:page])
  end

  def show
  end

  def new
    @certificate_symbol = CertificateSymbol.new
    @certificate_symbol.photos.build
  end

  def create
    @certificate_symbol = CertificateSymbol.new(symbol_params, :without_protection => true)

    if @certificate_symbol.save
      redirect_to certificate_symbol_path(@certificate_symbol)
    else
      render :new
    end
  end

  def edit
    if !@certificate_symbol.photos.first
      @certificate_symbol.photos.build
    end
  end

  def update
    if @certificate_symbol.update_attributes(symbol_params, :without_protection => true)
      redirect_to certificate_symbol_path(@certificate_symbol)
    else
      render :edit
    end
  end

  def destroy
    @certificate_symbol.destroy
    redirect_to certificate_symbols_path
  end

  private

  def certificate_symbol
    @certificate_symbol = CertificateSymbol.find(params[:id])
  end

  def symbol_params
    params.require(:certificate_symbol).permit(:certificate_code, :abbreviation, :name, :english_name, :description, :uses, photos_attributes: [:id, :image])
  end
end