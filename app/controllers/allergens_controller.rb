class AllergensController < ApplicationController

  def index
    @ingredients = Ingredient.all
  end

  def create
    @allergen = current_user.allergens.build(allergen_params, :without_protection => true)
    if @allergen.save
      respond_to do |format|
        format.html { redirect_to @allergens}
        format.js
      end
    else
      # TODO
     #  redirect_to allergens_path
    end
  end

  def destroy
    @allergen = current_user.allergens.find(params[:id])
    @allergen.destroy if @allergen
    respond_to do |format|
        format.html { redirect_to @allergens}
        format.js
    end
  end

  private

  def allergen_params
    params.permit(:ingredient_id)
  end

end