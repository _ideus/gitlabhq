class MineralsController < ApplicationController
  before_filter :mineral, except: [:index, :new, :create]

  def index
    @minerals = Mineral.all
    @minerals = @minerals.page(params[:page])
  end

  def show
  end

  def new
    @mineral = Mineral.new
  end

  def create
    @mineral = Mineral.new(mineral_params, :without_protection => true)

    if @mineral.save
      redirect_to mineral_path(@mineral)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @mineral.update_attributes(mineral_params, :without_protection => true)
      redirect_to mineral_path(@mineral)
    else
      render :edit
    end
  end

  def destroy
    @mineral.destroy
    redirect_to minerals_path
  end

  private

  def mineral
    @mineral = Mineral.find(params[:id])
  end

  def mineral_params
    params.require(:mineral).permit(:name, :alternative_name, :english_name, :description)
  end

end