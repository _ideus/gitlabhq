class CompaniesController < ApplicationController
  before_filter :company, except: [:index, :new, :create]

  def index
    @companies = Company.all
    @companies = @companies.page(params[:page])
  end

  def show
  end

  def new
    @company = Company.new
    @company.photos.build
  end

  def create
    @company = Company.new(company_params, :without_protection => true)

    if @company.save
      redirect_to company_path(@company)
    else
      render :new
    end
  end

  def edit
    if !@company.photos.first
      @company.photos.build
    end
  end

  def update
    if @company.update_attributes(company_params, :without_protection => true)
      redirect_to company_path(@company)
    else
      render :edit
    end
  end

  def destroy
    @company.destroy
    redirect_to companies_path
  end

  private

  def company
    @company = Company.find(params[:id])
  end

  def company_params
    params.require(:company).permit(:name, :address, :email, :phone, :fax, :website, :pib, photos_attributes: [:id, :image])
  end

end