class BrandsController < ApplicationController
  before_filter :brand, except: [:index, :new, :create]

  def index
    if params[:q]
      @brands = Brand.all.where("name LIKE ?", "#{params[:q]}%").order(:name).limit(10)
    else
      @brands = Brand.all
    end
    @brands = @brands.page(params[:page])
    respond_to do |format|
      format.html
      format.json  { render json: @brands, :only => [:id, :name]}
    end
  end

  def show
  end

  def new
    @brand = Brand.new
    @brand.photos.build
  end

  def create
    @brand = Brand.new(brand_params, :without_protection => true)
    if @brand.save
      redirect_to brand_path(@brand)
    else
      render :new
    end
  end

  def edit
    if !@brand.photos.first
      @brand.photos.build
    end
  end

  def update
    if @brand.update_attributes(brand_params, :without_protection => true)
      redirect_to brand_path(@brand)
    else
      render :edit
    end
  end

  def destroy
    @brand.destroy if @brand
    redirect_to brands_path
  end

  private

  def brand
    @brand = Brand.find(params[:id])
  end

  def brand_params
    params.require(:brand).permit(:name, :company_name, photos_attributes: [:id, :image])
  end

end