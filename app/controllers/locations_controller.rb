class LocationsController < ApplicationController
  class_attribute :request_geocoding_gatherer
  self.request_geocoding_gatherer = RequestGeocodingGatherer

  def index
    @current_location_by_ip = geocoded_request_information.current_location
    #@current_location_by_ip = request.location.to_json

    store_ids = Company.find_by(name: params[:c_name]).store_ids
    @stores = Location.where(addressable_type: "Store", addressable_id: store_ids).near(@current_location_by_ip)
  end

  private

  def store_params
    params.require(:location).permit(:address)
  end

  def geocoded_request_information
    request_geocoding_gatherer.new(request)
  end
end

