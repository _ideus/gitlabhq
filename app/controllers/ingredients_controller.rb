class IngredientsController < ApplicationController
  before_filter :ingredient, except: [:index, :new, :create]

  def index
    if params[:q]
      @ingredients = Ingredient.all.where("name LIKE ?", "#{params[:q]}%").order(:name).limit(10)
    else
      @ingredients = Ingredient.all
    end
    @ingredients = @ingredients.page(params[:page])
    respond_to do |format|
      format.html
      format.json  { render json: @ingredients, :only => [:id, :name]}
    end
  end

  def show
  end

  def new
    @ingredient = Ingredient.new
  end

  def create
    @ingredient = Ingredient.new(ingredient_params, :without_protection => true)

    if @ingredient.save
      redirect_to ingredient_path(@ingredient)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @ingredient.update_attributes(ingredient_params, :without_protection => true)
      redirect_to ingredient_path(@ingredient)
    else
      render :edit
    end
  end

  def destroy
    @ingredient.destroy
    redirect_to ingredients_path
  end

  private

  def ingredient
    @ingredient = Ingredient.find(params[:id])
  end

  def ingredient_params
    params.require(:ingredient).permit(:name, :alternative_name, :description)
  end

end