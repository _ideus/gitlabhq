class NutrientsController < ApplicationController
  before_filter :nutrient, except: [:index, :new, :create]

  def index
    @nutrients = Nutrient.all
    @nutrients = @nutrients.page(params[:page])
  end

  def show
  end

  def new
    @nutrient = Nutrient.new
  end

  def create
    @nutrient = Nutrient.new(nutrient_params, :without_protection => true)

    if @nutrient.save
      redirect_to nutrient_path(@nutrient)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @nutrient.update_attributes(nutrient_params, :without_protection => true)
      redirect_to nutrient_path(@nutrient)
    else
      render :edit
    end
  end

  def destroy
    @nutrient.destroy
    redirect_to nutrients_path
  end

  private

  def nutrient
    @nutrient = Nutrient.find(params[:id])
  end

  def nutrient_params
    params.require(:nutrient).permit(:name, :description)
  end

end