class Vitamin < ActiveRecord::Base
  has_many :product_vitamins
  has_many :products, :through => :product_vitamins
end
