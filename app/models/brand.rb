class Brand < ActiveRecord::Base
  has_many :photos, as: :imageable, dependent: :destroy
  has_many :products
  belongs_to :company
  accepts_nested_attributes_for :photos, allow_destroy: true

  validates :name, presence: true
  validates_uniqueness_of :name, :case_sensitive => false
  validate :company_identifiers

  def self.search(term)
    if term
      where("lower(name) LIKE ?", "%#{term.downcase}%")
    end
  end

  def company_name
    company.try(:name)
  end

  def company_name=(name)
    self.company = Company.find_by(name: name) if name.present?
  end

private

  # see http://stackoverflow.com/questions/2134188/validate-presence-of-one-field-or-another-xor
  def company_identifiers
    if not company_id.present?
      if not company_name.present?
        errors.add(:company_name, "cannot be empty.")
      end
    end
  end

end
