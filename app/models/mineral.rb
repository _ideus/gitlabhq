class Mineral < ActiveRecord::Base
  has_many :product_minerals
  has_many :products, :through => :product_minerals
end
