class ManufactureCompany < ActiveRecord::Base
  belongs_to :product
  belongs_to :company

  attr_accessible :product_id, :company_id

  def company_name
    company.try(:name)
  end

  def company_name=(name)
    self.company = Company.find_by(name: name) if name.present?
  end
end
