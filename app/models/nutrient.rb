class Nutrient < ActiveRecord::Base
  has_many :product_nutrients
  has_many :products, :through => :product_nutrients
end
