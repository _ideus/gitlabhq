class Product < ActiveRecord::Base
  is_impressionable
  include Searchable

  has_many :photos, as: :imageable, dependent: :destroy
  has_many :manufacture_companies
  has_many :manufacturers, :through => :manufacture_companies, :source => :company
  has_many :distribution_companies
  has_many :distributors, :through => :distribution_companies, :source => :company
  has_many :product_ingredients
  has_many :ingredients, :through => :product_ingredients
  has_many :product_allergens
  has_many :allergens, :through => :product_allergens
  has_many :product_nutrients
  has_many :nutrients, :through => :product_nutrients
  has_many :product_vitamins
  has_many :vitamins, :through => :product_vitamins
  has_many :product_minerals
  has_many :minerals, :through => :product_minerals

  has_many :users, :through => :favorites
  has_many :favorites
  belongs_to :brand

  has_many :product_rsymbols
  has_many :recycling_symbols, :through => :product_rsymbols
  has_many :product_csymbols
  has_many :certificate_symbols, :through => :product_csymbols

  accepts_nested_attributes_for :photos, :product_nutrients, :product_ingredients, :product_allergens, :product_vitamins, :product_minerals,
                                          :manufacture_companies, :distribution_companies,
                                          :product_rsymbols,
                                          allow_destroy: true

  attr_accessor :brand_name
  attr_accessor :ac_ingredient_ids
  attr_accessor :allergen_ingredient_ids
  attr_accessor :rsymbols
  attr_accessible :tag_list
  acts_as_taggable

  validates :name, :presence => true
  validates :description, :presence => true
  validates :barcode, format: { with: /\A([0-9]{8}|[0-9]{13})\Z/, message: "must be 13 or 8 digits"}, uniqueness: {message: "Barcode must be unique and can't be blank. "}, unless: :c_prod?

  def unsafe_for(user)
    return false if unsafe_ingredients_for(user).empty?
    return true
  end

  def unsafe_ingredients_for(user)
    product_ingredients.with_allergens(user.ingredient_ids)
  end

  def ac_ingredient_ids
    ingredient_ids.join(",")
  end

  def ac_ingredient_ids=(ids)
    product_ingredients.clear
    ids.split(",").select(&:present?).map do |ingredient_id|
      product_ingredients.build({ingredient_id: ingredient_id}, without_protection: true)
    end
  end

  def allergen_ingredient_ids
    ingredient_ids.join(",")
  end

  def allergen_ingredient_ids=(ids)
    product_allergens.clear
    ids.split(",").select(&:present?).map do |ingredient_id|
      product_allergens.build({ingredient_id: ingredient_id}, without_protection: true)
    end
  end

  def brand_name
    brand.try(:name)
  end

  def brand_name=(name)
    self.brand = Brand.find_by(name: name) if name.present?
  end

  def rsymbols
    recycling_symbol_ids.join(",")
  end

  def rsymbols=(ids)
    product_rsymbols.clear
    ids.split(",").select(&:present?).map do |recycling_symbol_id|
      product_rsymbols.build({recycling_symbol_id: recycling_symbol_id}, without_protection: true)
    end
  end

  def csymbols
    certificate_symbol_ids.join(",")
  end

  def csymbols=(ids)
    product_csymbols.clear
    ids.split(",").select(&:present?).map do |certificate_symbol_id|
      product_csymbols.build({certificate_symbol_id: certificate_symbol_id}, without_protection: true)
    end
  end
end
