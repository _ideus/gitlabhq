class ProductCsymbol < ActiveRecord::Base
  belongs_to :product
  belongs_to :certificate_symbol

  scope :with_csymbols, ->(certificate_symbol_ids) { where(certificate_symbol_id: certificate_symbol_ids) }
  scope :with_products, ->(product_ids) { where(product_id: product_ids) }
end
