class Store < ActiveRecord::Base
  belongs_to :company
  has_one :location, as: :addressable
end
