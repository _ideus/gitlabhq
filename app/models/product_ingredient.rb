class ProductIngredient < ActiveRecord::Base
  belongs_to :product
  belongs_to :ingredient

  scope :with_allergens, ->(ingredient_ids) { where(ingredient_id: ingredient_ids) }
  scope :with_products, ->(product_ids) { where(product_id: product_ids) }
end
