class Company < ActiveRecord::Base
  has_many :photos, as: :imageable, dependent: :destroy
  has_many :distribution_companies
  has_many :distributed_products, :through => :distribution_companies, :source => :product
  has_many :manufacture_companies
  has_many :manufactured_products, :through => :manufacture_companies, :source => :product
  has_many :brands
  has_many :stores

  accepts_nested_attributes_for :photos

  def self.search(term)
    if term
      where("lower(name) LIKE ?", "%#{term.downcase}%")
    end
  end
end