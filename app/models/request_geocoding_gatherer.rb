class RequestGeocodingGatherer
  def initialize(request)
    @request = request
  end

  def current_location
    return city if city
    return ''
  end

  private

  delegate :city, to: :location
  delegate :location, to: :@request
end

