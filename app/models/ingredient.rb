class Ingredient < ActiveRecord::Base
  has_many :product_ingredients
  has_many :products, through: :product_ingredients
  has_many :allergens
  has_many :users, through: :allergens

  def safe_for(user)
    return false if users.include?(user)
    return true
  end
end
