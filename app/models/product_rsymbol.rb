class ProductRsymbol < ActiveRecord::Base
  belongs_to :product
  belongs_to :recycling_symbol

  scope :with_rsymbols, ->(recycling_symbol_ids) { where(recycling_symbol_id: recycling_symbol_ids) }
  scope :with_products, ->(product_ids) { where(product_id: product_ids) }
end