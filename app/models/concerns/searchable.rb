module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    settings({
      index: {
        number_of_shards: 1,
        number_of_replicas: 0
      },
      analysis: {
        filter: {
          nGram_filter: {
            type: "nGram",
            min_gram: 2,
            max_gram: 20
          }
        },
        analyzer: {
          nGram_analyzer: {
            type: "custom",
            tokenizer: "whitespace",
            filter: [
              "lowercase",
              "asciifolding",
              "nGram_filter"
            ]
          },
          whitespace_analyzer: {
            type: "custom",
            tokenizer: "whitespace",
            filter: [
              "lowercase",
              "asciifolding"
            ]
          }
        }
      }
    })

    mapping({ _all: {index_analyzer: 'nGram_analyzer', search_analyzer: 'whitespace_analyzer'} }) do
      indexes :name, index: 'not_analyzed'

      indexes :manufacturers do
        indexes :name, index: 'not_analyzed'
      end

      indexes :distributors do
        indexes :name, index: 'not_analyzed'
      end

      indexes :tags do
        indexes :name, index: 'not_analyzed'
      end
    end

    # Set up callbacks for updating the index on model changes
    after_commit lambda { Indexer.perform_async(:index,  self.class.to_s, self.id) }, on: :create
    after_commit lambda { Indexer.perform_async(:update, self.class.to_s, self.id) }, on: :update
    after_commit lambda { Indexer.perform_async(:delete, self.class.to_s, self.id) }, on: :destroy
    after_touch  lambda { Indexer.perform_async(:update, self.class.to_s, self.id) }

    def as_indexed_json(options={})
      self.as_json(
        include: {
          manufacturers: { only: :name },
          distributors:  { only: :name },
          tags:          { only: :name }
        }
      )
    end

    def self.search(query, options={})

      _set_filters = lambda do |key, f|
        @search_definition[:filter][:and] ||= []
        @search_definition[:filter][:and]  |= [f]

        @search_definition[:aggs][key.to_sym][:filter][:and] ||= []
        @search_definition[:aggs][key.to_sym][:filter][:and]  |= [f]
      end

      # range aggregations are currently hack - their purpose is to act as noop filter
      # without which syntax for aggregations is not correct
      @search_definition = {
        query: {},

        filter: {},

        aggs: {
          manufacturers: {
            filter: {
              and: [
                {
                  range: {
                    created_at: {
                      gte: "2000-01-01"
                    }
                  }
                }
              ]
            },
            aggs: {
              manurs: {
                terms: {
                  field: "manufacturers.name"
                }
              }
            }
          },

          distributors: {
            filter: {
              and: [
                {
                  range: {
                    created_at: {
                      gte: "2000-01-01"
                    }
                  }
                }
              ]
            },
            aggs: {
              distrs: {
                terms: {
                  field: "distributors.name"
                }
              }
            }
          },
          tags: {
            filter: {
              and: [
                {
                  range: {
                    created_at: {
                      gte: "2000-01-01"
                    }
                  }
                }
              ]
            },
            aggs: {
              tgs: {
                terms: {
                  field: "tags.name"
                }
              }
            }
          }
        }
      }

      if options[:m]
        f = { term: { 'manufacturers.name' => options[:m] } }

        _set_filters.(:distributors, f)
        _set_filters.(:tags, f)
      end

      if options[:d]
        f = { term: { 'distributors.name' => options[:d] } }

        _set_filters.(:manufacturers, f)
        _set_filters.(:tags, f)
      end

      if options[:t]
        f = { term: { 'tags.name' => options[:t] } }

        _set_filters.(:manufacturers, f)
        _set_filters.(:distributors, f)
      end

      unless query.blank?
        @search_definition[:query] = {
          match: {
            _all: {
              query: query,
              operator: "and"
            }
          }
        }
      else
        @search_definition[:query] = { match_all: {} }
      end

      __elasticsearch__.search(@search_definition)

    end
  end
end