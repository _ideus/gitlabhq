class RecyclingSymbol < ActiveRecord::Base
  has_many :photos, as: :imageable, dependent: :destroy
  has_many :product_rsymbols
  has_many :products, :through => :product_rsymbols
  accepts_nested_attributes_for :photos
end
