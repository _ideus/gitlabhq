class Location < ActiveRecord::Base
  belongs_to :addressable, polymorphic: true

  class_attribute :geocoding_service
  self.geocoding_service = Geocoder

  geocoded_by :address

  after_create :set_coordinates

  def set_coordinates
    unless self.latitude and self.longitude
      self.latitude, self.longitude = geocoding_service.coordinates(address)
      self.save
    end
  end
end
