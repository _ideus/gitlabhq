class Allergen < ActiveRecord::Base
  belongs_to :user
  belongs_to :ingredient

  scope :with_ingredient, ->(ingredient_id) { where(ingredient_id: ingredient_id).first }
end
