class CertificateSymbol < ActiveRecord::Base
  has_many :photos, as: :imageable, dependent: :destroy
  has_many :product_csymbols
  has_many :products, :through => :product_csymbols
  accepts_nested_attributes_for :photos
end
