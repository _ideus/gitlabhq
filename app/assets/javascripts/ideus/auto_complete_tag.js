$(document).ready(function (){
  $("#tags").select2({
    tokenSeparators: [","],
    multiple: true,
    ajax: {
	    url: "/list_tags.json",
	    dataType: 'json',
	    data: function (term, page) {
        return {
          q: term,
          page: page
        };
      },
      results: function (data, page) {
        return {
          results: data
        };
      }
    },
    id: function(data) { return data.name; },
    formatResult: function (data) {
      return data.name;
    },
    formatSelection: function (data) {
      return data.name;
    },
    escapeMarkup: function (m) { return m; },
    initSelection : function (element, callback) {
      var data1 = [];
      jQuery(element.val().split(",")).each(function () {
          $.ajax({
              type: "get",
              url: "/existing_tags",
              async: false,
              dataType: 'json',
              data: { id: $("#product_id").val()},
              success: function(tag_list){
                $.each(tag_list, function(i, obj) {
                  data1.push({id: this.id, name: this.name});
                });
              }
          });
      });
      callback(data1);
    }
  });
});
