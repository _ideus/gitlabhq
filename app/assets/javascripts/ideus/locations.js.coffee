$ ->
  if ($('#map1').length > 0)
    lat_total = 0
    lon_total = 0
    count = 0
    for store in $(".stores li")
      lat = $(store).data("latitude")
      lon = $(store).data("longitude")
      if lat and lon
        lat_total = lat_total + lat
        lon_total = lon_total + lon
        count = count + 1
    lat_center = lat_total / count
    lon_center = lon_total / count

    map = L.map('map1', {
      center: [lat_center, lon_center],
      zoom: 11
    });

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors', max_zoom: 18
    }).addTo(map);

    for store in $(".stores li")
      lat = $(store).data("latitude")
      lon = $(store).data("longitude")
      id = $(store).data("id")
      if lat and lon
        popup = L.popup().setContent("<p>Store</p>")
        marker = L.marker([lat, lon]).bindPopup(popup).addTo(map)
