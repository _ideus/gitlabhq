$(function() {
  $(".btn").click(function(){
    $(".btn").removeClass('active-btn')
    $(this).addClass('active-btn');
  });
  $(".icon-table").hide();
  $(".table-switcher").click(function(){
    $("#icon-table").hide();
    $("#icon-bar-chart").show();
  });
  $(".chart-switcher").click(function(){
    $("#icon-bar-chart").hide();
    $("#icon-table").show();
  });

  $(".table-switcher-vitamin").click(function(){
    $("#icon-table-vitamin").hide();
    $("#icon-chart-vitamin").show();
  });
  $(".chart-switcher-vitamin").click(function(){
    $("#icon-chart-vitamin").hide();
    $("#icon-table-vitamin").show();
  });

  $(".table-switcher-mineral").click(function(){
    $("#icon-table-mineral").hide();
    $("#icon-chart-mineral").show();
  });
  $(".chart-switcher-mineral").click(function(){
    $("#icon-chart-mineral").hide();
    $("#icon-table-mineral").show();
  });
});