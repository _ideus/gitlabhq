$(document).ready(function (){
  $("#ac_ingredient").select2({
    tokenSeparators: [","],
    multiple: true,
    ajax: {
      url: "/ingredients.json",
      dataType: 'json',
      data: function (term, page) {
        return {
          q: term,
          page: page
        };
      },
      results: function (data, page) {
        return {
          results: data
        };
      }
    },
    formatResult: function (data) {
      return data.name;
    },
    formatSelection: function (data) {
      return data.name;
    },
    escapeMarkup: function (m) { return m; },
    initSelection: function (element, callback) {
      var data1 = [];
      jQuery(element.val().split(",")).each(function () {
          $.ajax({
              type: "get",
              url: "/list_ingredients",
              async: false,
              dataType: 'json',
              data: { id: $("#product_id").val()},
              success: function(ingredient){
                $.each(ingredient, function(i, obj) {
                  data1.push({id: this.id, name: this.name});
                });
              }
          });
      });
      callback(data1);
    }
  });

  $("#allergen_ingredient").select2({
    tokenSeparators: [","],
    multiple: true,
    ajax: {
      url: "/ingredients.json",
      dataType: 'json',
      data: function (term, page) {
        return {
          q: term,
          page: page
        };
      },
      results: function (data, page) {
        return {
          results: data
        };
      }
    },
    formatResult: function (data) {
      return data.name;
    },
    formatSelection: function (data) {
      return data.name;
    },
    escapeMarkup: function (m) { return m; },
    initSelection: function (element, callback) {
      var data1 = [];
      jQuery(element.val().split(",")).each(function () {
          $.ajax({
              type: "get",
              url: "/list_ingredients",
              async: false,
              dataType: 'json',
              data: { id: $("#product_id").val()},
              success: function(ingredient){
                $.each(ingredient, function(i, obj) {
                  data1.push({id: this.id, name: this.name});
                });
              }
          });
      });
      callback(data1);
    }
  });
});
