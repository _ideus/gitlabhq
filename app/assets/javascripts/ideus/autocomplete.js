$(function() {

  var companies = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: '/search/autocomplete2.json?type=companies&q=%QUERY'
  });

  companies.initialize();

  $('.auto_search_companies').typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'companies',
      displayKey: 'value',
      source: companies.ttAdapter()
    }
  );

  var brands = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: '/search/autocomplete2.json?type=brands&q=%QUERY'
  });

  brands.initialize();

  $('.auto_search_brands').typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'brands',
      displayKey: 'value',
      source: brands.ttAdapter()
    }
  );

});