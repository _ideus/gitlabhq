$(function() {
  var products = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: "/products/search?q=%QUERY"
  });

  products.initialize();

  $('#q').typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'products',
      displayKey: 'name',
      source: products.ttAdapter()
    }
  );
});
