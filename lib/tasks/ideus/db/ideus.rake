namespace :ideus do
  namespace :db do
    task populate_companies: :environment do
      Company.destroy_all
      Company.create({name: "Voli d.o.o.", address: "Bulevar Josipa Broza bb, 81000 Podgorica", email: "info@voli.me", phone: "+38220445000", fax: "+38220445038", website: "www.volivasvoli.com"}, without_protection: true)
      Company.create({name: "Mesopromet d.o.o. - Franca", address: "Rasovo b.b., 84000 Bijelo Polje", email: "mesopromet@t-com.me", phone: "+38250478488", fax: "+38250478588", website: "www.mesopromet.co.me", pib: "02063344"}, without_protection: true)
      Company.create({name: "Mercator-CG d.o.o. - Roda", address: "Put Radomira Ivanovića 2, 81000 Podgorica", email: "info@mercator.me", phone: "+38220442403", fax: "+38220442472", website: "www.roda.me"}, without_protection: true)
      Company.create({name: "Hard Discount Laković d.o.o.", address: "4.jul bb", email: "predrag.tomovic@hdl.co.me", phone: "+38220640330", fax: "+38220640695", website: "www.hdl.co.me"}, without_protection: true)
      Company.create({name: "Expo Commerce d.o.o.", address: "Industrijska zona bb, 85330 Kotor", email: "kotor@expocommerce.com", phone: "+38232311100", fax: "+38232311670", website: "www.expocommerce.com"}, without_protection: true)
      Company.create({name: "Domaća Trgovina d.o.o.", address: "Josipa Broza Tita 23A, 81000 Podgorica", phone: "+38269112112", website: "www.domacatrgovina.me"}, without_protection: true)
      Company.create({name: "Bar-kod d.o.o.", address: "Cetinjski put bb, 81000 Podgorica", email: "bar-kod@t-com.me", phone: "+38220260730", fax: "+38220260884", website: "www.bar-kod.com"}, without_protection: true)
      Company.create({name: "Neregelia d.o.o.", address: "Cetinjski put bb, 81000 Podgorica", email: "neregelia@nelt.com", phone: "+38220261920", fax: "+38220261930", website: "www.neregelia.me", pib: "02313707"}, without_protection: true)
      Company.create({name: "DMD Delta d.o.o.", address: "Donja Gorica bb, 81000 Podgorica", phone: "+38220891393", fax: "+38220891171", website: "www.deltadmd.rs", pib: "104764603"}, without_protection: true)
      Company.create({name: "Stadion d.o.o.", address: "Bulevar Ibrahima Dreševića br. 1, 81000 Podgorica", email: "stadion-doo@t-com.me", phone: "+38220622568", fax: "+38220622569", website: "www.stadion.co.me"}, without_protection: true)
      Company.create({name: "Crnagoracoop AB d.o.o.", address: "Landža bb, 81140 Danilovgrad", email: "crnagoracoop@t-com.me", phone: "+38220812287", fax: "+38220812464", website: "www.crnagoracoop.com"}, without_protection: true)
      Company.create({name: "13. jul plantaže a.d.", address: "Put Radomira Ivanovića 2, 81000 Podgorica", email: "info@plantaze.com", phone: "+38220658028", fax: "+38220658027", website: "www.plantaze.com"}, without_protection: true)
      Company.create({name: "Pirella d.o.o.", address: "Danilovgrad", email: "pirella@t-com.me", phone: "+38220883350", website: "www.pirellajuices.com"}, without_protection: true)
      Company.create({name: "Goranović d.o.o.", address: "Straševina bb, 81400 Nikšić", email: "info@migoranovic.com", phone: "+38277400000", fax: "+38277400003", website: "www.migoranovic.com", pib: "02169301"}, without_protection: true)
      Company.create({name: "Gradina Company d.o.o.", address: "Ibarska magistrala 50, 84310 Rožaje", email: "gradinaco@t-com.me", phone: "+38251271087", fax: "+38251271202", website: "www.gradina-company.com"}, without_protection: true)
      Company.create({name: "Šimšić Montmilk d.o.o. - Mljekara Lazine", address: "Lazine bb, 81140 Danilovgrad", email: "mljekaralazine@yahoo.com", phone: "+38220815312", fax: "+38220815313", website: "www.mljekaralazine.com"}, without_protection: true)
      Company.create({name: "Nika d.o.o", address: "Partizanski put bb, 81400 Nikšić", email: "nika-nk@t-com.me", phone: "+3822040222012", fax: "+3822040231302", website: "www.mljekara-nika.com"}, without_protection: true)
      Company.create({name: "Eko Meduza d.o.o.", address: "Industrijska bb, 84000 Bijelo Polje", email: "info@eko-meduza.me", phone: "+38250478086", fax: "+38250478602", website: "www.eko-meduza.me"}, without_protection: true)
      Company.create({name: "Šljukić Co d.o.o. - Mljekara Srna", address: "Ozrinići bb, 81400 Nikšić", email: "mljeksrna@t-com.me", phone: "+38240258161"}, without_protection: true)
      Company.create({name: "Zunjić Company d.o.o. - Pekara Anđela", address: "Podanje bb, 81140 Danilovgrad", email: "andjelagz@t-com.me", phone: "+38220881450", fax: "+38220882050", website: "www.andjela.me"}, without_protection: true)
      Company.create({name: "Uniprom pekara d.o.o.", address: "Podgorički put bb, 81140 Danilovgrad", email: "uniprompekara@t-com.me", phone: "+38240253323", fax: "+38240253305", website: "www.uniprompekara.com"}, without_protection: true)
      Company.create({name: "Bambi a.d.", address: "Đure Đakovića bb 12000 Požarevac, Srbija", email: "office@bambi.rs", phone: " +38112539800", fax: "+381112222555", website: "www.bambi.rs"}, without_protection: true)
      Company.create({name: "Klikovac d.o.o.", address: "Mahala bb, 81000 Podgorica", website: "www.klikovacdoo.com", pib: "02191601"}, without_protection: true)
      Company.create({name: "Nestlé d.o.o.", address: "Vevey, Vaud, Švajcarska", website: "www.nestle.com"}, without_protection: true)
      Company.create({name: "Ataco d.o.o.", address: "Bandići bb, 81410 Danilovgrad"}, without_protection: true)
      Company.create({name: "Soko Štark d.o.o.", address: "Kumodraška 249, 11000 Beograd, Srbija", website: "www.stark.rs"}, without_protection: true)
      Company.create({name: "Put-Gross d.o.o.", address: "Industrijska bb, 84000 Bijelo Polje", email: "put-gross@t-com.me", phone: "+38250480645", fax: "+38250478632", pib: "02454181"}, without_protection: true)
      Company.create({name: "Dijamant a.d.", address: "Temišvarski drum 14, 23000 Zrenjanin, Srbija", website: "www.dijamant.rs", phone: "+38123551050", fax: "+38123546347", email: "office@dijamant.rs"}, without_protection: true)
    end

    task populate_stores: :environment do
      Store.destroy_all
      c_shop_1 = Company.find_by(name: "Voli d.o.o.")
      store1 = Store.create({name: "Voli 1", company_id: c_shop_1.id}, without_protection: true)
      store2 = Store.create({name: "Voli 2", company_id: c_shop_1.id}, without_protection: true)
      store3 = Store.create({name: "Voli 4", company_id: c_shop_1.id}, without_protection: true)
      store4 = Store.create({name: "Voli 5", company_id: c_shop_1.id}, without_protection: true)
      store5 = Store.create({name: "Voli 6", company_id: c_shop_1.id}, without_protection: true)
      store6 = Store.create({name: "Voli 7 shop", company_id: c_shop_1.id}, without_protection: true)
      store7 = Store.create({name: "Voli 8", company_id: c_shop_1.id}, without_protection: true)
      store8 = Store.create({name: "Voli 9", company_id: c_shop_1.id}, without_protection: true)
      store9 = Store.create({name: "Voli 10", company_id: c_shop_1.id}, without_protection: true)
      store10 = Store.create({name: "Voli 11", company_id: c_shop_1.id}, without_protection: true)
      store11 = Store.create({name: "Voli 12 Caffe", company_id: c_shop_1.id}, without_protection: true)
      store12 = Store.create({name: "Voli 13", company_id: c_shop_1.id}, without_protection: true)
      store13 = Store.create({name: "Voli 14 shop", company_id: c_shop_1.id}, without_protection: true)
      store14 = Store.create({name: "Voli 15", company_id: c_shop_1.id}, without_protection: true)
      store15 = Store.create({name: "Voli 16 Caffe", company_id: c_shop_1.id}, without_protection: true)
      store16 = Store.create({name: "Voli 17", company_id: c_shop_1.id}, without_protection: true)
      store17 = Store.create({name: "Voli 18", company_id: c_shop_1.id}, without_protection: true)
      store18 = Store.create({name: "Voli 19 shop", company_id: c_shop_1.id}, without_protection: true)
      store19 = Store.create({name: "Voli 20", company_id: c_shop_1.id}, without_protection: true)
      store20 = Store.create({name: "Voli 21 shop", company_id: c_shop_1.id}, without_protection: true)
      store21 = Store.create({name: "Voli 22 shop", company_id: c_shop_1.id}, without_protection: true)
      store22 = Store.create({name: "Voli 23", company_id: c_shop_1.id}, without_protection: true)
      store23 = Store.create({name: "Voli 24", company_id: c_shop_1.id}, without_protection: true)
      store24 = Store.create({name: "Voli 25", company_id: c_shop_1.id}, without_protection: true)
      store25 = Store.create({name: "Voli 26 shop", company_id: c_shop_1.id}, without_protection: true)
      store26 = Store.create({name: "Voli 27", company_id: c_shop_1.id}, without_protection: true)
      store27 = Store.create({name: "Voli 28", company_id: c_shop_1.id}, without_protection: true)
      store28 = Store.create({name: "Voli 29", company_id: c_shop_1.id}, without_protection: true)
      store29 = Store.create({name: "Voli 30", company_id: c_shop_1.id}, without_protection: true)
      store30 = Store.create({name: "Voli 31", company_id: c_shop_1.id}, without_protection: true)
      store31 = Store.create({name: "Voli 32", company_id: c_shop_1.id}, without_protection: true)
      store32 = Store.create({name: "Voli 33", company_id: c_shop_1.id}, without_protection: true)
      store33 = Store.create({name: "Voli 34", company_id: c_shop_1.id}, without_protection: true)
      store34 = Store.create({name: "Voli 35", company_id: c_shop_1.id}, without_protection: true)
      store35 = Store.create({name: "Voli 36", company_id: c_shop_1.id}, without_protection: true)
      store36 = Store.create({name: "Voli 37", company_id: c_shop_1.id}, without_protection: true)
      store37 = Store.create({name: "Voli 38", company_id: c_shop_1.id}, without_protection: true)
      store38 = Store.create({name: "Voli 39", company_id: c_shop_1.id}, without_protection: true)
      store39 = Store.create({name: "Voli 40", company_id: c_shop_1.id}, without_protection: true)
      store40 = Store.create({name: "Voli 41", company_id: c_shop_1.id}, without_protection: true)
      store41 = Store.create({name: "Voli 42", company_id: c_shop_1.id}, without_protection: true)
      store42 = Store.create({name: "Voli 43", company_id: c_shop_1.id}, without_protection: true)
      store43 = Store.create({name: "Voli 44", company_id: c_shop_1.id}, without_protection: true)
      store44 = Store.create({name: "Voli 46", company_id: c_shop_1.id}, without_protection: true)
      store45 = Store.create({name: "Voli 47", company_id: c_shop_1.id}, without_protection: true)
      store46 = Store.create({name: "Voli 50 Super Voli", company_id: c_shop_1.id}, without_protection: true)
      store47 = Store.create({name: "Voli 48", company_id: c_shop_1.id}, without_protection: true)
      store48 = Store.create({name: "Voli 53", company_id: c_shop_1.id}, without_protection: true)
      store49 = Store.create({name: "Voli 51", company_id: c_shop_1.id}, without_protection: true)
      store50 = Store.create({name: "Voli 52 shop", company_id: c_shop_1.id}, without_protection: true)

      c_shop_2 = Company.find_by(name: "Goranović d.o.o.")
      g_store1 = Store.create({name: "Goranović d.o.o. prodavnica 1", company_id: c_shop_2.id}, without_protection: true)
      g_store2 = Store.create({name: "Goranović d.o.o. prodavnica 2", company_id: c_shop_2.id}, without_protection: true)
      g_store3 = Store.create({name: "Goranović d.o.o. prodavnica 3", company_id: c_shop_2.id}, without_protection: true)
      g_store4 = Store.create({name: "Goranović d.o.o. prodavnica 4", company_id: c_shop_2.id}, without_protection: true)

      c_shop_3 = Company.find_by(name: "13. jul plantaže a.d.")
      p_store1 = Store.create({name: "13. jul plantaže a.d. 1", company_id: c_shop_3.id}, without_protection: true)
      p_store2 = Store.create({name: "13. jul plantaže a.d. 2", company_id: c_shop_3.id}, without_protection: true)
    end

    task populate_locations: :environment do
      Location.destroy_all
      store1 = Store.find_by(name: "Voli 1")
      Location.create({addressable_id: store1.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Miljana Vukova br. 6", latitude: 42.441390, longitude: 19.264193}, without_protection: true)
      store2 = Store.find_by(name: "Voli 2")
      Location.create({addressable_id: store2.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Trg Nikole Kovačevića 10", latitude: 42.447609, longitude: 19.243119}, without_protection: true)
      store3 = Store.find_by(name: "Voli 4")
      Location.create({addressable_id: store3.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Vasa Raičkovića", latitude: 42.444809, longitude: 19.250981}, without_protection: true)
      store4 = Store.find_by(name: "Voli 5")
      Location.create({addressable_id: store4.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Kralja Nikole 9", latitude: 42.436733, longitude: 19.261109}, without_protection: true)
      store5 = Store.find_by(name: "Voli 6")
      Location.create({addressable_id: store5.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Bulevar SKOJ-a 1", latitude: 42.448953, longitude: 19.23062}, without_protection: true)
      store6 = Store.find_by(name: "Voli 7 shop")
      Location.create({addressable_id: store6.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Bulevar SKOJ-a 1", latitude: 42.448953, longitude: 19.23062}, without_protection: true)
      store7 = Store.find_by(name: "Voli 8")
      Location.create({addressable_id: store7.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Petrovac", address: "Nika Anđusa bb", latitude: 42.206843, longitude: 18.93987}, without_protection: true)
      store8 = Store.find_by(name: "Voli 9")
      Location.create({addressable_id: store8.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Cijevna bb", latitude: 42.36621, longitude: 19.226792}, without_protection: true)
      store9 = Store.find_by(name: "Voli 10")
      Location.create({addressable_id: store9.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Bulevar Josipa Broza", latitude: 42.429507, longitude: 19.271852}, without_protection: true)
      store10 = Store.find_by(name: "Voli 11")
      Location.create({addressable_id: store10.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Zlatica bb", latitude: 42.465275, longitude: 19.297029}, without_protection: true)
      store11 = Store.find_by(name: "Voli 12 Caffe")
      Location.create({addressable_id: store11.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Bulevar Josipa Broza", latitude: 42.429507, longitude: 19.271852}, without_protection: true)
      store12 = Store.find_by(name: "Voli 13")
      Location.create({addressable_id: store12.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Kotor", address: "Radanovići bb", latitude: 42.357085, longitude: 18.761516}, without_protection: true)
      store13 = Store.find_by(name: "Voli 14 shop")
      Location.create({addressable_id: store13.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Kotor", address: "Radanovići bb", latitude: 42.357085, longitude: 18.761516}, without_protection: true)
      store14 = Store.find_by(name: "Voli 15")
      Location.create({addressable_id: store14.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Bar", address: "Obala 13. Jula", latitude: 42.093231, longitude: 19.096082}, without_protection: true)
      store15 = Store.find_by(name: "Voli 16 Caffe")
      Location.create({addressable_id: store15.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Bar", address: "Obala 13. Jula", latitude: 42.093231, longitude: 19.096082}, without_protection: true)
      store16 = Store.find_by(name: "Voli 17")
      Location.create({addressable_id: store16.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Sutomore", address: "Mirošica 1", latitude: 42.142529, longitude: 19.046119}, without_protection: true)
      store17 = Store.find_by(name: "Voli 18")
      Location.create({addressable_id: store17.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Igalo", address: "Jadranski put bb", latitude: 42.457059, longitude: 18.494604}, without_protection: true)
      store18 = Store.find_by(name: "Voli 19 shop")
      Location.create({addressable_id: store18.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Igalo", address: "Jadranski put bb", latitude: 42.457059, longitude: 18.494604}, without_protection: true)
      store19 = Store.find_by(name: "Voli 20")
      Location.create({addressable_id: store19.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Nikšić", address: "Bulevar 13. Jula br. 6", latitude: 42.76812, longitude: 18.94943}, without_protection: true)
      store20 = Store.find_by(name: "Voli 21 shop")
      Location.create({addressable_id: store20.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Nikšić", address: "Bulevar 13. Jula br. 6", latitude: 42.76812, longitude: 18.94943}, without_protection: true)
      store21 = Store.find_by(name: "Voli 22 shop")
      Location.create({addressable_id: store21.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Bar", address: "Obala 13. Jula", latitude: 42.093231, longitude: 19.096082}, without_protection: true)
      store22 = Store.find_by(name: "Voli 23")
      Location.create({addressable_id: store22.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Virpazar", address: "Virpazar bb", latitude: 42.246787, longitude: 19.090609}, without_protection: true)
      store23 = Store.find_by(name: "Voli 24")
      Location.create({addressable_id: store23.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Bulevar Josipa Broza bb", latitude: 42.429507, longitude: 19.271852}, without_protection: true)
      store24 = Store.find_by(name: "Voli 25")
      Location.create({addressable_id: store24.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Bijelo Polje", address: "Industrijska bb", latitude: 43.046201, longitude: 19.76719}, without_protection: true)
      store25 = Store.find_by(name: "Voli 26 shop")
      Location.create({addressable_id: store25.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Bijelo Polje", address: "Industrijska bb", latitude: 43.046201, longitude: 19.76719}, without_protection: true)
      store26 = Store.find_by(name: "Voli 27")
      Location.create({addressable_id: store26.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Kolašin", address: "Trg Borca", latitude: 42.824157, longitude: 19.521052}, without_protection: true)
      store27 = Store.find_by(name: "Voli 28")
      Location.create({addressable_id: store27.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Žabljak", address: "Narodnih heroja br. 1", latitude: 43.154722, longitude: 19.121444}, without_protection: true)
      store28 = Store.find_by(name: "Voli 29")
      Location.create({addressable_id: store28.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Mosorska 3", latitude: 42.465275, longitude: 19.297029}, without_protection: true)
      store29 = Store.find_by(name: "Voli 30")
      Location.create({addressable_id: store29.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Blaža Jovanovića bb", latitude: 42.447899, longitude: 19.245138}, without_protection: true)
      store30 = Store.find_by(name: "Voli 31")
      Location.create({addressable_id: store30.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Serdara Jola Piletića bb", latitude: 42.446922, longitude: 19.255723}, without_protection: true)
      store31 = Store.find_by(name: "Voli 32")
      Location.create({addressable_id: store31.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Herceg Novi", address: "Njegoševa br. 1", latitude: 42.451439, longitude: 18.536}, without_protection: true)
      store32 = Store.find_by(name: "Voli 33")
      Location.create({addressable_id: store32.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Herceg Novi", address: "Baošići bb", latitude: 42.438572, longitude: 18.619644}, without_protection: true)
      store33 = Store.find_by(name: "Voli 34")
      Location.create({addressable_id: store33.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Tivat", address: "21. novembra bb.", latitude: 42.429211, longitude: 18.69941}, without_protection: true)
      store34 = Store.find_by(name: "Voli 35")
      Location.create({addressable_id: store34.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Herceg Novi", address: "Nikole Ljubibratića 28", latitude: 42.460083, longitude: 18.528291}, without_protection: true)
      store35 = Store.find_by(name: "Voli 36")
      Location.create({addressable_id: store35.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Igalo", address: "Novo Mjesto bb.", latitude: 42.463057, longitude: 18.509372}, without_protection: true)
      store36 = Store.find_by(name: "Voli 37")
      Location.create({addressable_id: store36.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Radovići", address: "Radovići bb", latitude: 42.396392, longitude: 18.671542}, without_protection: true)
      store37 = Store.find_by(name: "Voli 38")
      Location.create({addressable_id: store37.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Meljine", address: "Braće Pedišića br. 24", latitude: 42.454352, longitude: 18.562773}, without_protection: true)
      store38 = Store.find_by(name: "Voli 39")
      Location.create({addressable_id: store38.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Herceg Novi", address: "Hercegovačke brigade br. 51", latitude: 42.452647, longitude: 18.535409}, without_protection: true)
      store39 = Store.find_by(name: "Voli 40")
      Location.create({addressable_id: store39.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "4. jula bb", latitude: 42.420966, longitude: 19.257042}, without_protection: true)
      store40 = Store.find_by(name: "Voli 41")
      Location.create({addressable_id: store40.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Beogradska bb", latitude: 42.442552, longitude: 19.27386}, without_protection: true)
      store41 = Store.find_by(name: "Voli 42")
      Location.create({addressable_id: store41.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Tuzi bb", latitude: 42.36701, longitude: 19.328419}, without_protection: true)
      store42 = Store.find_by(name: "Voli 43")
      Location.create({addressable_id: store42.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Berane", address: "Miljana Tomičića 29", latitude: 42.850166, longitude: 19.868355}, without_protection: true)
      store43 = Store.find_by(name: "Voli 44")
      Location.create({addressable_id: store43.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Moskovska br. 8", latitude: 42.441947, longitude: 19.247971}, without_protection: true)
      store44 = Store.find_by(name: "Voli 46")
      Location.create({addressable_id: store44.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Pljevlja", address: "Miloša Tošića bb", latitude: 43.352044, longitude: 19.358436}, without_protection: true)
      store45 = Store.find_by(name: "Voli 47")
      Location.create({addressable_id: store45.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Kotor", address: "Radanovići bb", latitude: 42.357085, longitude: 18.761516}, without_protection: true)
      store46 = Store.find_by(name: "Voli 50 Super Voli")
      Location.create({addressable_id: store46.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Podgorica", address: "Cetinjski put bb", latitude: 42.437936, longitude: 19.232087}, without_protection: true)
      store47 = Store.find_by(name: "Voli 48")
      Location.create({addressable_id: store47.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Mojkovac", address: "Trgovačka bb", latitude: 42.962311, longitude: 19.57888}, without_protection: true)
      store48 = Store.find_by(name: "Voli 53")
      Location.create({addressable_id: store48.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Berane", address: "Dragiše Radevića bb", latitude: 42.851956, longitude: 19.869329}, without_protection: true)
      store49 = Store.find_by(name: "Voli 51")
      Location.create({addressable_id: store49.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Rožaje", address: "Ibarska 2", latitude: 42.836254, longitude: 20.156778}, without_protection: true)
      store50 = Store.find_by(name: "Voli 52 shop")
      Location.create({addressable_id: store50.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Rožaje", address: "Ibarska 2", latitude: 42.836254, longitude: 20.156778}, without_protection: true)

      g_store = Store.find_by(name: "Goranović d.o.o. prodavnica 1")
      Location.create({addressable_id: g_store.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Nikšić", address: "Bulevar 13. Jula br. 6", latitude: 42.73802, longitude: 18.91923}, without_protection: true)
      p_store = Store.find_by(name: "13. jul plantaže a.d. 1")
      Location.create({addressable_id: p_store.id, addressable_type: "Store", country: "Montenegro", postal_code: nil, city: "Bijelo Polje", address: "Industrijska bb", latitude: 43.043201, longitude: 19.76729}, without_protection: true)
    end

    task populate_minerals: :environment do
      Mineral.destroy_all
      File.open("public/minerals.txt") do |minerals|
        minerals.read.each_line do |mineral|
          name, alternative_name, english_name, reference_value, reference_unit, description = mineral.chomp.split("|").map {|part| part.strip }
          Mineral.create({name: name, alternative_name: alternative_name, english_name: english_name, reference_value: reference_value.to_f,
            reference_unit: reference_unit, description: description}, without_protection: true)
        end
      end  
    end

    task populate_nutrients: :environment do
      Nutrient.destroy_all
      File.open("public/nutrients.txt") do |nutrients|
        nutrients.read.each_line do |nutrient|
          name, reference_value, reference_unit, description = nutrient.chomp.split("|").map {|part| part.strip }
          Nutrient.create({name: name, reference_value: reference_value.to_f,
            reference_unit: reference_unit, description: description}, without_protection: true)
        end
      end
    end

    task populate_vitamins: :environment do
      Vitamin.destroy_all
      File.open("public/vitamins.txt") do |vitamins|
        vitamins.read.each_line do |vitamin|
          name, alternative_name, english_name, reference_value, reference_unit, description = vitamin.chomp.split("|").map {|part| part.strip }
          Vitamin.create({name: name, alternative_name: alternative_name, english_name: english_name, reference_value: reference_value.to_f,
            reference_unit: reference_unit, description: description}, without_protection: true)
        end
      end  
    end

    task populate_tags: :environment do
      ActsAsTaggableOn::Tag.destroy_all
      ActsAsTaggableOn::Tag.create({name: "slatkiši"})
      ActsAsTaggableOn::Tag.create({name: "grickalice"})
      ActsAsTaggableOn::Tag.create({name: "meso"})
      ActsAsTaggableOn::Tag.create({name: "mesne prerađevine"})
      ActsAsTaggableOn::Tag.create({name: "riba"})
      ActsAsTaggableOn::Tag.create({name: "mlijeko i mliječni proizvodi"})
      ActsAsTaggableOn::Tag.create({name: "jaja"})
      ActsAsTaggableOn::Tag.create({name: "smrznuti program"})
      ActsAsTaggableOn::Tag.create({name: "sir i namazi"})
      ActsAsTaggableOn::Tag.create({name: "alkoholna pića"})
      ActsAsTaggableOn::Tag.create({name: "bezalkoholna pića"})
      ActsAsTaggableOn::Tag.create({name: "osnovne životne namirnice"})
      ActsAsTaggableOn::Tag.create({name: "hljeb i peciva"})
      ActsAsTaggableOn::Tag.create({name: "svježe voće i povrće"})
      ActsAsTaggableOn::Tag.create({name: "kafa"})
      ActsAsTaggableOn::Tag.create({name: "čaj"})
      ActsAsTaggableOn::Tag.create({name: "polugotova i gotova jela"})
      ActsAsTaggableOn::Tag.create({name: "začin"})
      ActsAsTaggableOn::Tag.create({name: "zdrava i dijetalna hrana"})
      ActsAsTaggableOn::Tag.create({name: "gotove torte i kolači"})
    end

    task populate_symbols: :environment do
      Photo.where(imageable_type: "RecyclingSymbol").destroy_all
      RecyclingSymbol.destroy_all

      sym1 = RecyclingSymbol.create({recycling_code: 1, abbreviation: "PET", name: "Polietilen-tereftalat", english_name: "Polyethylene terephthalate", description: "Najraširenija vrsta plastike koja se uglavnom koristi za boce za vodu, sokove, posude za prehrambene proizvode i ostalu ambalažu.", uses: "Reciklira se da bi se proizvela poliesterna vlakna, flastera, mekih boca za pića, termo-izolacionih ploča."}, without_protection: true)
      Photo.create({image: File.open("public/images/sym1.png"), imageable: sym1}, without_protection: true)
      Photo.create({image: File.open("public/images/sim11.png"), imageable: sym1}, without_protection: true)
      Photo.create({image: File.open("public/images/sim12.png"), imageable: sym1}, without_protection: true)
      
      sym2 =RecyclingSymbol.create({recycling_code: 2, abbreviation: "HDPE", name: "Polietilen velike gustine", english_name: "High-density polyethylene", description: "Koristi se za boce za mlijeko, sokove, jogurt, vodu, deterdžente i druge hemijske preparate.", uses: "Reciklira se da bi se proizvele boce, kutije za namirnice, kante za recikliranje, poljoprivredne cijevi, šoljice, plastično drvo."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim2.png"), imageable: sym2}, without_protection: true)
      Photo.create({image: File.open("public/images/sim21.png"), imageable: sym2}, without_protection: true)

      sym3 =RecyclingSymbol.create({recycling_code: 3, abbreviation: "PVC", name: "Polivinil-hlorid", english_name: "Polyvinyl chloride", description: "Koristi se za boce za deterdžente, šampone i druge hemijske preparate, kablove i drugi građevinski materijal.", uses: "Reciklira se da bi se proizvele cijevi, ograde i boce koje se ne koriste u prehrani."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim3.png"), imageable: sym3}, without_protection: true)
      Photo.create({image: File.open("public/images/sim31.png"), imageable: sym3}, without_protection: true)
      Photo.create({image: File.open("public/images/sim32.png"), imageable: sym3}, without_protection: true)

      sym4 =RecyclingSymbol.create({recycling_code: 4, abbreviation: "LDPE", name: "Polietilen male gustine", english_name: "Low-density polyethylene", description: "Koristi se za fleksibilne boce, vrećice za hleb, smrznutu hranu.", uses: "Reciklira se da bi se proizvele plastične vrećice, razni kontejneri, razne boce, cijevi i razna laboratorijska oprema."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim4.png"), imageable: sym4}, without_protection: true)
      Photo.create({image: File.open("public/images/sim41.png"), imageable: sym4}, without_protection: true)
      Photo.create({image: File.open("public/images/sim42.png"), imageable: sym4}, without_protection: true)
      
      sym5 =RecyclingSymbol.create({recycling_code: 5, abbreviation: "PP", name: "Polipropilen", english_name: "Polypropylene", description: "Koristi se za boce za jogurt, sirupe, kečap, medicinske boce, zatvarače za boce.", uses: "Reciklira se u razne dijelove za vozila i industrijska vlakna."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim5.png"), imageable: sym5}, without_protection: true)
      Photo.create({image: File.open("public/images/sim51.png"), imageable: sym5}, without_protection: true)
      Photo.create({image: File.open("public/images/sim52.png"), imageable: sym5}, without_protection: true)
      
      sym6 =RecyclingSymbol.create({recycling_code: 6, abbreviation: "PS", name: "Polistiren", english_name: "Polystyrene", description: "Koristi se za čvršću ambalažu, tanjire, čaše, kutije za lijekove.", uses: "Reciklira se u raznu kancelarijsku opremu, igračke, videokasete i kutije, izolacijske ploče, ugostiteljska pomagala i proširene polistirenske proizvode."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim6.png"), imageable: sym6}, without_protection: true)
      Photo.create({image: File.open("public/images/sim61.png"), imageable: sym6}, without_protection: true)
      Photo.create({image: File.open("public/images/sim62.png"), imageable: sym6}, without_protection: true)
      
      sym7 =RecyclingSymbol.create({recycling_code: 7, abbreviation: "O", name: "Sve druge plastike", english_name: "All other plastics", description: "Ostala plastika: akrilik, polikarbonat, najlon, fiberglas, poliaktid."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim7.png"), imageable: sym7}, without_protection: true)
      Photo.create({image: File.open("public/images/sim71.png"), imageable: sym7}, without_protection: true)
      Photo.create({image: File.open("public/images/sim72.png"), imageable: sym7}, without_protection: true)
      
      sym8 =RecyclingSymbol.create({recycling_code: 20, abbreviation: "C PAP", name: "Karton (talasasti)", english_name: "Cardboard", description: "Kartoni mase iznad 500 g/m2. Obično izrađeni od drvenjače, hemijske drvenjače i starog papira. Ovo je višeslojni materijal dobijen od raznih vlakana lijepljenjem i pritiskom.", uses: "Koristi se za izradu različitih omota ambalaža, vreća, vrećica, kartonskih kutija i sl."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim020.png"), imageable: sym8}, without_protection: true)
      
      sym9 =RecyclingSymbol.create({recycling_code: 21, abbreviation: "PAP", name: "Karton (ravni)", english_name: "Other paper", description: "Glavne osobine su: visoka otpornost, neznatna lomljivost, obostrana glatkost.", uses: "Upotrebljava se za izradu nesloživih i nepresvučenih kutija (prilikom presvlačenja dolazi do krivljenja što je njen najveći nedostatak). Tanki sivi karton je slabijeg kvaliteta."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim021.png"), imageable: sym9}, without_protection: true)
      
      sym10 =RecyclingSymbol.create({recycling_code: 22, abbreviation: "PAP", name: "Papir", english_name: "Paper", description: "Papir", uses: "Skupljanjem starog papira znatno se smanjuje količina otpada koja se odlaže na deponijama. Preradom starog papira dobija se sirovina za proizvodnju novog proizvoda, takvom preradom štedi se energija, jer proizvodnja papira iz otpadnog papira iziskuje i do 50% manje energije nego izrada papira iz drveta."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim022.png"), imageable: sym10}, without_protection: true)

      sym11 =RecyclingSymbol.create({recycling_code: 40, abbreviation: "FE", name: "Čelik", english_name: "Steel", description: "Čelik je sveprisutan u našim dnevnim životima, čelik, dugotrajan je i može se reciklirati 100 posto. Bez obzira na to koliko puta se čelik reciklira, on ostaje jak i izdržljiv. Kao jedan od samo nekoliko magnetskih metala koji se lako odvaja od ostalih metala, čelik se lako može reciklirati.", uses: "Proces recikliranja zadržava inherentne bitne karakteristike materijala, što znači da je reciklirani čelik jednako jak i izdržljiv kao i čelik koji je izrađen od željezne rude."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim040.png"), imageable: sym11}, without_protection: true)
      
      sym12 =RecyclingSymbol.create({recycling_code: 41, abbreviation: "ALU", name: "Aluminijum", english_name: "Aluminium", description: "Aluminijum se koristi kao ambalaža za većinu sokova.", uses: "Recikliranje aluminijuma je tako efikasno da se u roku manjem od 60 dana može skupiti stara ambalaža, rastopiti, napraviti nova koja je već na policama. Aluminijum ne gubi svoja svojstva prilikom recikliranja, sto znači da se gotovo beskonačno može reciklirati. Isplativost recikliranja aluminijuma je u tome što se uštedi 95% energije u odnosu na dobijanje aluminija iz boksita."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim041.png"), imageable: sym12}, without_protection: true)
      Photo.create({image: File.open("public/images/sim0411.png"), imageable: sym12}, without_protection: true)
      
      sym13 =RecyclingSymbol.create({recycling_code: 50, abbreviation: "FOR", name: "Drvo", english_name: "Wood", description: "Drvo je obnovljiva sirovina, a proizvodnja upotrebnih predmeta od drveta u većini slučajeva zahtijeva manje energije nego kod ostalih materijala.", uses: "U razvijenom svijetu vrlo je česta upotreba ambalaže od drveta a takva ambalaža može se višekratno koristiti, iako se njeno stanje može s vremenom pogoršati. Svo meko i tvrdo drvo, palete kutije, pakovanja predmeta, lesonit i OSB ploče, šperploča, laminati, MDF ploča."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim050.png"), imageable: sym13}, without_protection: true)

      sym14 =RecyclingSymbol.create({recycling_code: 51, abbreviation: "FOR", name: "Pluta", english_name: "Cork", description: "Pluta (latinski cortex) se dobija od kore hrasta plutnjaka te je stoga 100% regenerativno i može se u potpunosti reciklirati. Ova neobična sirovina prirodno je elastična poput ljudske kože.", uses: "Zbog svojstva da ne propušta plinove i vodu, pluta predstavlja dobru zaštitu od transpiracije, a zbog elastičnosti i čvrstoće njenih ćelija štiti i od mehaničkih povreda. Budući da su joj ćelije ispunjene vazduhom, pluta ima i važnu ulogu u izjednačavanju mogućih temperaturnih promjena."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim051.png"), imageable: sym14}, without_protection: true)

      sym15 =RecyclingSymbol.create({recycling_code: 71, abbreviation: "GLS", name: "Bezbojno staklo", english_name: "Clear glass", description: "Proces recikliranja stakla je proces pretvaranja odbačenog stakla u korisni proizvod. Staklo dolazi u različitim bojama, ali tri najčešće su: bezbojno (prozirno) staklo, zeleno staklo i smeđe staklo. Staklo čini veliki dio kućnog i industrijskog otpada prema svojoj težini i gustoći.", uses: "Stakleni otpad u gradskom otpadu se sastoji od staklenih boca, staklene robe i posuđa, sijalica i drugih stvari."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim071.png"), imageable: sym15}, without_protection: true)

      sym16 =RecyclingSymbol.create({recycling_code: 72, abbreviation: "GLS", name: "Zeleno staklo", english_name: "Green glass", description: "", uses: ""}, without_protection: true)
      Photo.create({image: File.open("public/images/sim072.png"), imageable: sym16}, without_protection: true)
      
      sym17 =RecyclingSymbol.create({recycling_code: 80, name: "Papir / Karton", english_name: "Paper and cardboard", description: "Raznovrsni materijali"}, without_protection: true)
      sym18 =RecyclingSymbol.create({recycling_code: 81, abbreviation: "PapPet", name: "Papir / Plastika", english_name: "Paper / Plastic", uses: "Kutije za hranu kućnih ljubimaca, pakovanja za sladoled, kartonske kutije i sl."}, without_protection: true)
      sym19 =RecyclingSymbol.create({recycling_code: 82, name: "Papir i karton / Aluminijum", english_name: "Paper and cardboard / Aluminium"}, without_protection: true)
      sym20 =RecyclingSymbol.create({recycling_code: 83, name: "Papir i karton / Bijeli lim", english_name: "Paper and cardboard / Tinplate"}, without_protection: true)

      sym21 =RecyclingSymbol.create({recycling_code: 84, abbreviation: "C/PAP", name: "Karton / Plastika / Aluminijum", english_name: "Paper and cardboard / Plastic / Aluminium", description: "Kombinacija navedenih materijala.", uses: "Kartonske kutije, pakovanje cigareta, pakovanja žvaka, Tetra Brik, kutije za sokove i dr."}, without_protection: true)
      Photo.create({image: File.open("public/images/sim084.png"), imageable: sym21}, without_protection: true)
      
      sym22 =RecyclingSymbol.create({recycling_code: 85, name: "Papir i karton / Plastika / Aluminijum / Bijeli lim", english_name: "Paper and cardboard / Plastic / Aliminium / Tinplate"}, without_protection: true)
      sym23 =RecyclingSymbol.create({recycling_code: 90, name: "Plastika / Aluminijum", english_name: "Plastic / Aliminium"}, without_protection: true)
      sym24 =RecyclingSymbol.create({recycling_code: 91, name: "Plastika / Bijeli lim", english_name: "Plastic / Tinplate"}, without_protection: true)
      sym25 =RecyclingSymbol.create({recycling_code: 92, name: "Plastika / Raznovrsni metali", english_name: "Plastic / Various metals"}, without_protection: true)
      sym26 =RecyclingSymbol.create({recycling_code: 95, name: "Staklo / Plastika", english_name: "Glass / Plastic"}, without_protection: true)
      sym27 =RecyclingSymbol.create({recycling_code: 96, name: "Staklo / Aluminijum", english_name: "Glass / Aluminium"}, without_protection: true)
      sym28 =RecyclingSymbol.create({recycling_code: 97, name: "Staklo / Bijeli lim", english_name: "Glass / Tinplate"}, without_protection: true)
      sym29 =RecyclingSymbol.create({recycling_code: 98, name: "Staklo / Raznovrsni metali", english_name: "Glass / Various metals"}, without_protection: true)

      sym30 =RecyclingSymbol.create({name: "Mebijusova petlja", english_name: "Mobius loop", description: "Univerzalni simbol za reciklažu."}, without_protection: true)
      Photo.create({image: File.open("public/images/meb.jpg"), imageable: sym30}, without_protection: true)
      
      sym31 =RecyclingSymbol.create({name: "Tidyman", english_name: "Tidyman", description: "Originalni naziv ovog simbola je engleski “THE TIDYMAN”, što u slobodnom prijevodu označava čovjeka-osobu koja brine o okolini, odnosno osobu koja ne zagađuje okolinu time što otpatke baca u za to predviđena mjesta (kante za otpatke, kontejnere i sl.)"}, without_protection: true)
      Photo.create({image: File.open("public/images/tid.jpg"), imageable: sym31}, without_protection: true)
      
      sym32 =RecyclingSymbol.create({name: "Zelena tačka", english_name: "Green dot", description: "Ovim simbolom proizvođač obavještava potrošača da je on ili uvoznik, platio propisanu novčanu naknadu na račun organizacije za upravljanje ambalažnim otpadom. Ta bi organizacija tada u pravilu trebala osigurati pravilno upravljanje ambalažnim otpadom."}, without_protection: true)
      Photo.create({image: File.open("public/images/dot.jpg"), imageable: sym32}, without_protection: true)
      
      sym33 =RecyclingSymbol.create({name: "Recikliraj!", english_name: "Recycle Now", description: "Osnovno značenje simbola RECIKLIRAJ! (eng. Recycle Now, Recycle Mark), kao što i njegov naziv govori – je poziv na akciju, odnosno apel cjelokupnome društvu da RECIKLIRA u što većoj mogućoj mjeri."}, without_protection: true)
      Photo.create({image: File.open("public/images/rec.jpg"), imageable: sym33}, without_protection: true)
    end

    task populate_certificates: :environment do
      Photo.where(imageable_type: "CertificateSymbol").destroy_all
      CertificateSymbol.destroy_all

      sym5 = CertificateSymbol.create({abbreviation: "HALAL", name: "Halal hrana - zahtjevi i mjere, BAS 1049:2010", english_name: "Halal certified", description: "Halal standardom je utvrđeno što je dozvoljeno a što zabranjeno muslimanima, kako se sertificira i provjerava primjena odredbi halal standarda, kako se vrši klanje halal životinja, kako se označavaju halal proizvodi te koji su aditivi halal a koji nisu. Halal standard počiva na preventivnoj proaktivnoj metodi, kompatibilan je sa drugim međunarodnim standardima koji uređuju oblast kvalitetnog upravljanja proizvodnjom  (ISO, HACCP, IFS, BRC, GLOBAL, GAP i drugi).", uses: "Halal serifikat predstavlja neophodan uslov za izvoz hrane na tržište islamskih zemalja."}, without_protection: true)
      Photo.create({image: File.open("public/images/halal.jpg"), imageable: sym5}, without_protection: true)

      sym6 = CertificateSymbol.create({abbreviation: "DOBRO", name: "Dobro iz Crne Gore", description: "Privredna komora je u septembru 2008. pokrenula kampanju Dobro iz Crne Gore, čiji je cilj da se brendiraju domaći proizvodi kako bi bili što konkurentniji na tržištu. Baziran je na rezultatima analiza Privredne komore o robnoj razmjeni Crne Gore sa inostranstvom tokom prethodnih godina, u kojima je konstatovan veliki, kontinuirani i rastući spoljnotrgovinski deficit u razmjeni roba i usluga.", uses: "Osnovni cilj registrovanja kolektivnog žiga DOBRO IZ CRNE GORE je prepoznatljivost, kvalitet i uspješan plasman crnogorskih proizvoda na domaćem i inostranom tržištu."}, without_protection: true)
      Photo.create({image: File.open("public/images/dobro.jpg"), imageable: sym6}, without_protection: true)

      sym7 = CertificateSymbol.create({abbreviation: "Euro Leaf", name: "Euro Leaf - EU oznaka za organske proizvode", english_name: "Euro Leaf - EU organic logo", description: "U zemljama Evropske unije proizvodi iz organskog uzgoja koji se nađu u prodaji, moraće nositi jedinstveni znak, novi EU logo za orgnasku hranu, koji svojim prepoznatljivim dizajnom treba prenijeti dvije poruke: priroda i Evropa, ali, još važnije, osigurati potpuno povjerenje da kupuju robu koja je proizvedena u skladu sa novim regulativama organske proizvodnje u EU.", uses: "Uz mnoge druge odredbe u okviru novih regulativa EU, proizvodi mogu biti označeni kao organski samo ako je najmanje 95 % sastojaka organski proizvedeno. Proizvod ne smije ni u jednom svom dijelu sadržavati GMO."}, without_protection: true)
      Photo.create({image: File.open("public/images/euro-leaf.jpg"), imageable: sym7}, without_protection: true)

      sym8 = CertificateSymbol.create({abbreviation: "Bez GMO", name: "ARGE - Bez GMO", english_name: "ARGE - GMO Free", description: "Austrijski serfitikat koji garantuje da hrana i hrana za životinje nije genetski modifikovana.", uses: "Više od 2.200 proizvoda u Evropi nosi ovaj znak kvaliteta 'proizvedeno bez genetskog inženjeringa'"}, without_protection: true)
      Photo.create({image: File.open("public/images/gmo.png"), imageable: sym8}, without_protection: true)

      sym9 = CertificateSymbol.create({abbreviation: "V oznaka", name: "V oznaka - Evropska vegetarijanska oznaka", english_name: "V label - European vegetarian label", description: "Sadašnju V-oznaku kreirao je italijanski umjetnik prof. Bruno Nascimben i predstavio za upotrebu širom svijeta na kongresu EVU 1985. u Cerviai, Italija. Od tada su mnoge vegetarijanske i veganske organizacije usvojile logo tako da je postao najkorišteniji vegetarijanski simbol širom svijeta. Tržište za vegetarijanske proizvode i alternative mesu ubrzano raste i postoji jasna potreba za pomoći potrošačima da prepoznaju ove proizvode.", uses: "Registrirana zaštitna oznaka Europske vegetarijanske unije (EVU) nudi potrošaču sigurnost, doslovno na prvi pogled bez potrebe da detaljno proučava listu sastojaka. Međutim, V-oznaka nije od pomoći samo vegetarijancima. V-oznaka namijenjena je vegetarijancima, veganima, alergičarima i svima koji se žele hraniti zdravije i paze na sastojke proizvoda koje svakodnevno kupuju. U vrijeme masovnog uzgoja životinja, afera s hranom i BSE, sve više ljudi želi znati što tačno jede."}, without_protection: true)
      Photo.create({image: File.open("public/images/vegan.jpg"), imageable: sym9}, without_protection: true)

      sym10 = CertificateSymbol.create({abbreviation: "AMA znak za organske proizvode", name: "AMA znak za organske proizvode bez naznake porijekla", english_name: "Austrian Organic Label without indication of origin", description: "Austrijska nacionalna oznaka za organske proizvode. U skladu je sa propisima EU, kao i sa austrijskim kodeksom hrane (poglavlje 8).", uses: "Organski proizvodi u EU. Najzastupljeniji u Austriji."}, without_protection: true)
      Photo.create({image: File.open("public/images/ama.jpg"), imageable: sym10}, without_protection: true)

      sym11 = CertificateSymbol.create({abbreviation: "Bio", name: "Bio znak", english_name: "Bio sign", description: "Bio je njemački bio zaštitni znak, koji pokazuje da proizvodi moraju sadržavati najmanje 95% sastojaka koji su izvedeni iz organske proizvodnje. Takođe, tu je provjera preostalih 5% sastojaka u proizvodima.", uses: "Organski proizvodi u EU. Najzastupljeniji u Njemačkoj."}, without_protection: true)
      Photo.create({image: File.open("public/images/bio.jpg"), imageable: sym11}, without_protection: true)

      sym12 = CertificateSymbol.create({abbreviation: "Bez glutena", name: "Bez glutena", english_name: "Gluten free", description: "Postoje namirnice, koje po svojoj prirodi ne sadrže gluten (npr. soja, heljda, kukuruz, riža...). Označavanje i sertifikovanje proizvoda oznakom 'bez glutena' je prvenstveno namijenjen za lakši odabir osoba s posebnim prehrambenim potrebama.", uses: "Hrana napravljena od žitarica ili hibrida žitarica, koje prirodno ne sadrže gluten, ne smije sadržati više od 20mg glutena po kilogramu suve materije."}, without_protection: true)
      Photo.create({image: File.open("public/images/gluten.jpeg"), imageable: sym12}, without_protection: true)

      # sym13 = CertificateSymbol.create({abbreviation: "Vegan", name: "Vegan", english_name: "Vegan", description: "Postoje namirnice, koje po svojoj prirodi ne sadrže gluten (npr. soja, heljda, kukuruz, riža...). Označavanje i sertifikovanje proizvoda oznakom 'bez glutena' je prvenstveno namijenjen za lakši odabir osoba s posebnim prehrambenim potrebama.", uses: "Hrana napravljena od žitarica ili hibrida žitarica, koje prirodno ne sadrže gluten, ne smije sadržati više od 20mg glutena po kilogramu suve materije."}, without_protection: true)
      # Photo.create({image: File.open("public/images/vegan2.jpg"), imageable: sym13}, without_protection: true)

      sym13 = CertificateSymbol.create({abbreviation: "VSA", name: "Odobrilo Vegetarijansko Društvo", english_name: "Vegetarian Society Approved", description: "Ovo je prvo vegetarijansko društvo (Vegetarian Society) u svijetu, osnovano je 1847. godine u Velikoj Britaniji. Logo VSA je najrasprostranjeniji vegetarijanski logo na svijetu.", uses: "Vegetarijanska hrana."}, without_protection: true)
      Photo.create({image: File.open("public/images/vegsoc.jpg"), imageable: sym13}, without_protection: true)
    end

    task populate_ingredients: :environment do
      Ingredient.destroy_all
      Ingredient.create({name: "limunska kiselina", alternative_name: "E330", description: "Limunska ili askorbinska kiselina je antioksidans i regulator kiselosti. To je bijela, kristalna stvar, veoma kisjelog ukusa, lako topljiva u vodi. Industrijski se može proizvesti iz limuna ili iz šećera fermentacijom pomoću plijesni Aspergillus niger. Koristi se u proizvodnji bezalkoholnih napitaka, za aromatiziranje i zakiseljavanje raznih namirnica (krastavci, paprika), u pekarskoj, mesnoj industriji, smrznutoj ribi, voću i povrću. Služi kao konzervans jer spriječava oksidaciju i vrenje.
        "}, without_protection: true)
      Ingredient.create({name: "šećer", description: "Šećer je praskašti ili kristalni ugljeni hidrat karakterističnog slatkog ukusa. Rastvorljiv je u vodi i koristi se u ishrani. Dobija se uglavnom od šećerne repe i šećerne trske.
        "}, without_protection: true)
      Ingredient.create({name: "voda", description: "Voda je bistra i tečnost bez mirisa i boje, a osvježavajućeg je ukusa.
        "}, without_protection: true)
      Ingredient.create({name: "so", description: "Kuhinjska so je praškasti ili kristalni mineral, jedan od najvažnijih u ljudskoj ishrani. Dobija se iz morske vode u solanama ili iskopavanjem u rudnicima. Glavni je izvor iona Na+ i Cl- za ljude i životinje. Ioni Na+ imaju ključnu ulogu u mnogim fiziološkim procesima od održanja stalnog krvnog pritiska do održanja rada nervnog sustava. Zato je unošenje kuhinjske soli neophodno za život. Ljudske dnevne potrebe za unos kuhinjske soli iznose oko 5g.
        "}, without_protection: true)
      Ingredient.create({name: "mlijeko", description: "Mlijeko je biološka tečnost mliječnih žlijezda sisara. To je tećnost bijele boje složenog sastava. Sastav mlijeka varira, što ovisi od više faktora, kao sto su: uzrast, rasa, prehrana životinje, godišnje doba i sl. Mlijeko je prirodna hrana bogata kalcijumom, bjelančevinama, vitaminima i mineralima, prijeko potrebnim za rast i pravilno funkcioniranje ljudskog organizma.
        "}, without_protection: true)
      Ingredient.create({name: "soja", description: "Soja je biljka mahunarka visoke hranjive vrijednosti. Postoje razne sorte soje, koje se razlikuju po obliku zrna, boji, okusu i hemijskim svojstvima. Sadrži vrlo veliki postotak masti - 19,9%, ugljikohidrata - 30,2% i bjelančevina - 36,5%, zatim vitamine A i B grupe. U prehrani i industriji upotrebljava se u raznim oblicima kao što su: sojino mlijeko, sir poznat kao tofu, brašno, ulje, umak, briketi itd.
        "}, without_protection: true)
      Ingredient.create({name: "sojin lecitin ", alternative_name: "E322", description: "Sojin lecitin je proizvod koji se dobija prilikom prerade sojinog zrna u proteinske proizvode i sirovo degumirano ulje. Odličan je emulgator, sredstvo za dispergovanje čvrstih čestica, stabilizator pjene, sredstvo kvašenja i opuštanja, kao sredstvo za kontrolu kristalizacije. Ima izrazita dijetetska i terapeutska svojstva i preporučuje sa kao dodatak ishrani.
        "}, without_protection: true)
      Ingredient.create({name: "aroma", description: "Aroma je karakteristično svojstvo hrane koje je čovjek prepoznao i počeo koristiti još u prahistoriji. U prehrambenoj industriji se dodaju prirodne i sintetisane arome proizvodima radi poboljšanja njihovog mirisa i ukusa.
        "}, without_protection: true)
      Ingredient.create({name: "gluten", description: "Gluten je lepljiva, elastična bjelančevinasta supstanca koja se nalazi u pšenici (žitu), brašnu, raži, ječmu, ovasu i zobi koja je često kontaminirana zbog toga što se najčešće uzgaja uz pšenicu. Gluten čini tijesto ljepljivim i on daje elastičnost tijestu. Osim u spomenutim žitaricama, gluten se nalazi svuda oko nas, sadrži ga većina proizvoda – pizza, tjestenina, industrijska hrana, pa čak i ruževi za usne. Vrlo je bitna prehrambena komponenta, jer su sjemenke žita inače siromašne bjelančevinama.
        "}, without_protection: true)
      Ingredient.create({name: "jaja", description: "Kokošija jaja su sastojak mnogih prehrambenih proizvoda, bilo da su slana ili slatka. Sadrže sve značajne amino kiseline potrebne čovjeku, kao i nekoliko vitamina i minerala, uključujući vitamine A, B2, B9, B6, B12, gvožđe, holin, kalcijum, fosfor i kalijum.
        "}, without_protection: true)
      Ingredient.create({name: "pšenično brašno", description: "Pšenično brašno je jedno od najvažnijih namirnica u europskoj i američkoj kulturi, i bitan sastojak u većini vrsta peciva i tijesta. Sadrži bjelančevinu nazvanu gluten, koje daje tijestu elastičnu strukturu. To omogućuju zadržavanje mjehurića, što daje prozračan i mekan konačni proizvod, prikladan za hljeb, kolače i slično.
        "}, without_protection: true)
      Ingredient.create({name: "obrano mlijeko u prahu", description: "Obrano mlijeko u prahu je mliječni proizvod dobijen isparavanjem mlijeka. Jedna svrha sušenja je čuvanje mlijeka jer u prahu ima daleko duži rok trajanja od tekućeg mlijeka i ne treba ga čuvati na hladnom. Druga svrha je smanjenje volumena što ga čini ekonomičnijim za transport na veće udaljenosti. Sadrži od 1,0-1,5 % mliječne masti. Ima vrlo široku primjenu u prehrambenoj industriji, od proizvodnje hrane za odojčad, preko slatkiša, do pekarske industrije.
        "}, without_protection: true)
      Ingredient.create({name: "kakao maslac", description: "Kako maslac se dobija hladnim presovanjem kakao zrna. Ima delikatan miris i ukus čokolade. Često se koristi kao sastojak u prirodnim proizvodima za njegu kože (sapuni, losioni, kreme) i za izradu čokolade, kolača itd.
        "}, without_protection: true)
      Ingredient.create({name: "kikiriki", description: "Za ljudsku uporabu koristi se sjeme kikirikija. Kikiriki je bogat bjelančevinama, vlaknima, mineralima, željezom, cinkom i vitaminima E i K. Kikiriki ne sadrži holesterol. Upotrebljava se kao dodatak kolačima, za proizvodnju maslaca i ulja od kikirikija i drugo.Ljekovite i hranjive osobine kikirikija blagotvorno utječu na polnu potenciju, poboljšavaju pamćenje i pažnju, poboljšavaju sluh, koristan je kod jake iznemoglosti i teških oboljenja. On sadrži vitamine grupe B koji daju sjaj kosi i umirujuće deluju na nervni sistem, kao vitamine grupe E koji stimuliraju funkciju spolnih žlijezda.
        "}, without_protection: true)
      Ingredient.create({name: "lješnici", description: "Lješnik je bogat proteinima, nezasićenim mastima, tiaminom i vitaminom B6. Seme preko sebe ima tanku, tamno braon ljusku koja je gorkog ukusa i koja se neretko skida pre kuvanja.
        "}, without_protection: true)
      Ingredient.create({name: "bademi", description: "Badem predstavlja hranljivu grickalicu i važan sastojak zdravih poslastica. Odličan je izvor bjelančevina i sadrži antikancerogene materije. Sadrži vitamine B2, B3 i E, folnu kiselinu i minerale (kalcijum, magnezijum, fosfor, kalijum, cink). Važan je izvor vitamina E - antioksidanta koji štiti od delovanja slobodnih radikala. Koristan je za jačanje, formiranje novih krvnih zrnaca i popravljanje hemoglobina, važan je i za funkcije mozga, nervnog sistema, kostiju, srca i jetre, protiv gorušice, suvižne kiseline, suve kože itd.
        "}, without_protection: true)
      Ingredient.create({name: "pistaći", description: "Pistaći su veoma bogati kalijumom, kalcijumom, fosforom, magnezijumom, a i vitaminima A, C i E kao i folatima. Vrlo je efikasan kod regulicije nivoa holesterola.
        "}, without_protection: true)
      Ingredient.create({name: "biljno ulje", description: "Biljna ulja se dobijaju iz plodova ili semenja velikog broja biljaka. Bilja ulja, zbog prisustva velikih količina polinezasićenih masnih kiselina imaju visoku biološku aktivnost i niz antisklerotičnih svojstava. U njima apsolutno nema holesterola. Polinezasićene masne kiseline doprinose izbacivanju holesterola iz organizma, putem pretvaranja holesgerina u lako rastvorljiva jedinjenja. Doprinese elastičnosti krvnih sudova. Učestvuju u razmeni masti i holesterina, na osnovu čega se pretvaraju u vitamine (vitamin F). Biljna ulja su sastavni deo svakodnevne ishrane zbog svoje hranljive vrijednosti.
        "}, without_protection: true)
      Ingredient.create({name: "biljna mast", description: "Biljne masti danas se sve češće upotrebljavaju u kulinarstvu, dobra su i veoma zdrava zamjena za masti životinjskog porijekla. Uglavnom se prave bez holesterola, imaju neutralan ukus i ugodan miris, čvrstu konzistenciju i karakteristično su bijele boje i mazive strukture. Prirodne biljne masti, kao što su palmina i kokosova mast, kod nas se uglavnom uvoze, a biljne masti domaćeg porijekla proizvode se postupkom hidrogenizacije jestivih biljnih ulja.
        "}, without_protection: true)
      Ingredient.create({name: "kakaova maslac", description: "Kakaov maslac, glavni sastojak čokolade, dobija se iz kakao zrna.
        "}, without_protection: true)
      Ingredient.create({name: "glukozni sirup", description: "Glukozni sirup je koncentrisani rastvor šećera dobijenih kiselinskom ili enzimskom hidrolizom skroba. U prehrambenoj industriji se koristi kao zaslađivač kod proizvodnje konditorskih proizvoda.
        "}, without_protection: true)
      Ingredient.create({name: "surutka u prahu", description: "Surutka u prahu se dobija iz neprevrele slatke surutke pri proizvodnji sira posle izdvajanja kazeina. Sadrži proteine sa visokovrednim amino kiselinama mlijeka, mliječni šećer - laktozu, vitamine iz mlijeka i mineralne materije mlijeka.
        "}, without_protection: true)
      Ingredient.create({name: "kakaov prah smanjene masti", description: "Kakaov prah smanjene masti je kakaov prah koji sadrži manje od 20% kakaovog maslaca (suve materije).
        "}, without_protection: true)
      Ingredient.create({name: "kakaov prah", description: "Kakaov prah je proizvod dobijen od očišćenih, oljuštenih i prženih kakaovih zrna, koji sadrži najmanje 20% kakaovog maslaca (suve materije) i najviše 9% vode.
        "}, without_protection: true)
      Ingredient.create({name: "pšenica", description: "Pšenica je biljka koja se uzgaja širom svijeta. Pšenična zrna su glavni prehrambeni proizvod koji se koristi za dobijanje brašna za hljeb, kolače, tjesteninu itd; i za dobijanje piva, alkohola, votke i biogoriva.
        "}, without_protection: true)
      Ingredient.create({name: "dekstroza", description: "Dekstroza predstavlja prost šećer koji se izuzetno brzo apsorbuje. Dekstroza je poznata među bodibilderima i sportistima jer izaziva skok insulina i učestvuje u obnovi glikogena u mišićnim ćelijama (rezervi ugljenih hidrata) koji se troši treningom.
        "}, without_protection: true)
      Ingredient.create({name: "kvasac", description: "Kvasac je sitna jednostanična gljivica koja pripada grupi mikroorganizama.Zbog visokog sadržaja bjelančevina i vitamina B-skupine upotrebljava se kao dodatak prehrambenim proizvodima. Kvasac se najčešće upotrebljava za dizanje tijesta, kako bi ono povećalo volumen, razvilo strukturu te stvorilo specifičan ukus i miris.
        "}, without_protection: true)
      Ingredient.create({name: "monogliceridi i digliceridi masnih kiselina", alternative_name: "E471", description: "Prirodni emulgatori i stabilizatori, sredstva za homogenizaciju masti i vode, materije protiv pjenjenja. Namirnicama se uglavnom dodaju po pravilu quantum satis. Smatraju se bezopasnima.
        "}, without_protection: true)
      Ingredient.create({name: "susam", description: "Sjeme susama sadrži 45-60% ulja koje se upotrebljava za kuvanje, pripremanje salata i za proizvodnju margarina. Sjeme je također bogato proteinima. Upotrebljava se u pekarstvu za posipanje peciva i za pripremanje grickalica.
        "}, without_protection: true)
      Ingredient.create({name: "natrijev hidrogenkarbonat ", alternative_name: "E500(ii)", description: "U kulinarstvu je poznat pod nazivom soda bikarbona. Predstavlja bijeli kristalni prah, slabo topljiv u vodi. Koristi se u pekarstvu kao osnovni sastojak u proizvodnji raznih praškova za dizanje tijesta, praškova za pecivo i pečenje, u kozmetici, u proizvodnji pjenušavih pića, u proizvodnji umjetnih mineralnih voda (soda voda), u proizvodnji tekstila, papira, keramike, kao punilo vatrogasnih aparata, itd.
        "}, without_protection: true)
      Ingredient.create({name: "guar guma", alternative_name: "E412", description: "Prirodni biljni zgušnjivač, sredstvo za želiranje, emulgator i stabilizator. Dobija se iz sjemena indijske biljke Cyamoposis tetragonolobus. Po svojemu hemijskom sastavu polisaharid građen od jedinica manoze i laktoze. Pospješuje probavu. Dopuštena je upotreba u ekološkoj proizvodnji hrane. U pojedinim slučajevima može uzrokovati alergije. U većim količinama može uzrokovati mučnine, nadutost i grčeve. Namirnicama se dodaje po pravilu quantum satis. Smatra se bezopasnom.
        "}, without_protection: true)
      Ingredient.create({name: "beta karoten", alternative_name: "E160a", description: "Beta-karoten je biljni pigment koji tijelo pretvara u vitamin A (retinol). Vitamin A je tijelu potreban za dobar vid i zdravlje očiju, za snažan imunitet, zdravu kožu i sluznicu. Dok je vitamin A u velikim količinama toksičan, tijelo će u vitamin A pretvoriti samo onoliko beta-karotena koliko mu je potrebno. Beta-karoten je antioksidans – štiti tijelo od slobodnih radikala, molekula koje ga oštećuju i time smanjuje rizik od mnogih kroničnih bolesti. Ipak, i beta-karoten uzet iz suplemenata može biti štetan, dok onaj koji dobivamo kroz prehranu nije štetan.
        "}, without_protection: true)
      Ingredient.create({name: "kalijev sorbat", alternative_name: "E202", description: "Prirodno sredstvo za konzervisanje. Kalijeva so sorbinske kiseline. Smatra se bezopasnim.
        "}, without_protection: true)
      Ingredient.create({name: "maltodekstrin", description: "Maltodekstrin je oligosaharid koji se koristi kao prehrambeni aditiv. Lako se vari. Brzo se apsorbuje poput glukoze i može da bude bilo umjereno sladak ili skoro bez ukusa. Koristi se u proizvodnji bezalkoholnih pića i bombona i mnogih drugih prehrambenih proizvoda.
        "}, without_protection: true)
      Ingredient.create({name: "amonijev hidrogenkarbonat", alternative_name: "E503", description: "Prirodna sredstva za regulisanje kisjelosti i sprečavanje zgrudnjavanja. Dopušteni su u ekološkoj proizvodnji namirnica. Ako se unose u organizam u velikim dozama (6-8 g), amonijeve soli mogu povećati kisjelost krvi i izlučivanje esencijalnih mineralnih materija, međutim namirnice ne sadrže tako velike količine. Smatraju se bezopasnima.
        "}, without_protection: true)
      Ingredient.create({name: "mliječna mast", description: "Mliječna mast je najskuplji sastojak mlijeka, jer od ukupne energetske vrijednosti mlijeka na mast otpada prosječno 54%, a drugi razlog je i njena biološka vrijednost u odnosu na druge masti. Kaže se da mlijeko prosječno sadrži 3,8% mliječne masti, ali to je sastojak koji podliježe najvećim varijacijama.
        "}, without_protection: true)
      Ingredient.create({name: "laktoza", description: "Laktoza je disaharid koji se sastoji od D-glukoze i D-galaktoze. Kravlje mlijeko sadrži prosječno 4,8% laktoze. Ona čini oko 37% suve materije mlijeka, što čini da mliječnog šećera ima više nego ostalih sastojaka pojedinačno.
        "}, without_protection: true)
      Ingredient.create({name: "kukuruzni skrob", description: "Kukuruzni skrob dobija se iz zrna kukuruza mokrom preradom. U obliku je finog, bijelog praha, bez ukusa i mirisa. Ne rastvara se u hladnoj vodi. Uspješno se upotrebljava u industriji hrane za dobijanje brašna, hljeba i peciva, za proizvodnju keksa i praškastih pudinga, lijekova, dječje hrane, supica i umaka.
        "}, without_protection: true)
      Ingredient.create({name: "mononatrijev glutaminat", alternative_name: "E621", description: "Prirodni pojačivač ukusa i zamjena za so. Proizvodi se biotehnološkim postupkom ili hemijskom sintezom. Može se proizvesti tehnikom genetičkog inženjerstva, ali procjena učinaka tako dobijenog natrijevog glutamata još nije završena. Izbjegavati! http://e-brojevi.udd.hr/621.htm
        "}, without_protection: true)
      Ingredient.create({name: "med", description: "Med je sladak i gust sok koji pčele prave skupljajući nektar. Med je i najsavršeniji proizvod prirode, u njemu se nalaze gotovo svi sastojci koji grade ljudski organizam.
        "}, without_protection: true)
      Ingredient.create({name: "fruktoza", description: "Fruktoza je ugljeni hidrat. Poznata je pod nazivom i voćni šećer. Slađa je od saharoze odnosno od bijelog šećera. Ima energetsku vrednost,pa ga ne bi trebalo da koriste gojazne osobe, a zahteva znatno manju količinu insulina od količine koja je potrebna za razgradnju saharoze(šećer koji se koristi u domaćinstvu), pa se često preporučuje osobama koje su oboljele od dijabetesa.
        "}, without_protection: true)
      Ingredient.create({name: "ksantan guma", alternative_name: "E415", description: "Vještački zgušnjivač, stabilizator i sredstvo za želiranje. Dobiva se fermentacijom kukuruznog šećera (smjesa glukoze i sukroze) uz pomoć bakterija Xanthomonas camestris. Moguća je proizvodnja iz genetički modifikovanih sirovina, ali konačnu procjenu učinaka tako dobivene ksantan gume još nije moguće dati. U većim dozama djeluje laksativno. Dopuštena je u ekološkoj proizvodnji hrane. Namirnicama se dodaje po pravilu quantum satis. Smatra se bezopasnom.
        "}, without_protection: true)
      Ingredient.create({name: "kukuruzno brašno", description: "Brašno nastalo mljevenjem kukuruznog zrna. Koristi se kod pripreme hljeba i kolača. Kod proizvodnje hljeba najčešće se miješa s drugim brašnom, jer nema glutena pa ni svojstva dizanja tijesta.
        "}, without_protection: true)
      Ingredient.create({name: "mliječna čokolada", description: "Svjetlosmeđe boje i mekane konzistencije. Dobija se od kakaa i šećera uz dodatak mlijeka, pavlake, pavlake u prahu, mlijeka u prahu, mliječne masti i lecitina. Može sadržati i kakao maslac. Mliječna čokolada sadrži oko 25% masti i do 55% šećera.
        "}, without_protection: true)
      Ingredient.create({name: "mliječna kiselina", alternative_name: "E270", description: "Prirodni regulator kisjelosti i sredstvo za konezrvisanje. Dopuštena je u ekološkoj proizvodnji hrane. Namirnicama se smije dodavati po pravilu quantum satis. Smatra se bezopasnom.
        "}, without_protection: true)
      Ingredient.create({name: "pasterizirano mlijeko", description: "Mlijeko podvrgnuto postupku toplotne obrade na temperaturi od 71,7°C u trajanju od 15-20 sekundi, a zatim naglo ohlađeno u trajanju manjem od jedne sekunde. Pasterizirano mlijeko nije kuvano. Oslobođeno je svih vegetativnih oblika patogenih mikroorganizama.
        "}, without_protection: true)
      Ingredient.create({name: "sukraloza", alternative_name: "E955", description: "Vještački zaslađivač koji je 500 do 600 puta slađi od šećera saharoze. Proizvodi se kloriranjem saharoze. Nema nikakvu energijsku vrijednost jer se najvećim dijelom iz organizma izlučuje nepromijenjena. Iako se smatra bezopasnom, često konzumiranje nije preporučljivo.
        "}, without_protection: true)
      Ingredient.create({name: "glicerol", alternative_name: "E422", description: "Prirodni stabilizator, sredstvo za zadržavanje vlage i povećanje volumena. U prirodi se nalazi u životinjskim i biljnim mastima i uljima. Dopušten je u ekološkoj proizvodnji hrane. Namirnicama se dodaje po pravilu quantum satis. Smatra se bezopasnim.
        "}, without_protection: true)
      Ingredient.create({name: "aspartam", alternative_name: "E951", description: "Vještački zaslađivač i pojačivač ukusa. Naučna istraživanja pokazuju da aspartam može biti vrlo štetan za zdravlje. U simptome pretjerane konzumacije aspartama ubrajaju se omamljenost, glavobolja, umor, vrtoglavice, povraćanje, ubrzani rad srca, gojenje, razdražljivost, uznemirenost, gubitak pamćenja, zamagljeno vidno polje, osip, slijepilo, bol u zglobovima, depresija, grčevi, pobačaji, neplodnost, zavisnost, slabost, gubitak sluha. Pri zagrijavanju hrane koja sadrži aspartam može doći do stvaranja toksičnog diketopiperazina. Izbjegavati!"
        }, without_protection: true)
    end

   task populate_products: :environment do
      Product.destroy_all
      p1 = Product.create({name: "Plazma", barcode: "8600043001280", description: "plazma keks", net_weight: "600", reference_unit: "g", origin: "Srbija"}, without_protection: true)
      cd1 = Company.find_by(name: "Klikovac d.o.o.")
      DistributionCompany.create({product_id: p1.id, company_id: cd1.id}, without_protection: true)
      cm1 = Company.find_by(name: "Bambi a.d.")
      ManufactureCompany.create({product_id: p1.id, company_id: cm1.id}, without_protection: true)
      cs1 = CertificateSymbol.find_by(abbreviation: "HALAL")
      ProductCsymbol.create({product_id: p1.id, certificate_symbol_id: cs1.id}, without_protection: true)

      p2 = Product.create({name: "Mlevena Plazma", barcode: "8600043001105", description: "mljeveni plazma keks", net_weight: "300", reference_unit: "g", origin: "Srbija"}, without_protection: true)
      DistributionCompany.create({product_id: p2.id, company_id: cd1.id}, without_protection: true)
      ManufactureCompany.create({product_id: p2.id, company_id: cm1.id}, without_protection: true)
      ProductCsymbol.create({product_id: p2.id, certificate_symbol_id: cs1.id}, without_protection: true)

      p3 = Product.create({name: "Thomy – Majonez delikatesni", barcode: "7610100061455", description: "majonez", net_weight: "165", reference_unit: "g", origin: "Švajcarska"}, without_protection: true)
      cd3 = Company.find_by(name: "Bar-kod d.o.o.")
      DistributionCompany.create({product_id: p3.id, company_id: cd3.id}, without_protection: true)
      cm3 = Company.find_by(name: "Nestlé d.o.o.")
      ManufactureCompany.create({product_id: p3.id, company_id: cm3.id}, without_protection: true)

      p4 = Product.create({name: "Najlepše želje – Čokolada & keks", barcode: "8600939344057", description: "čokolada sa keksom", net_weight: "90", reference_unit: "g", origin: "Srbija"}, without_protection: true)
      cd4 = Company.find_by(name: "Ataco d.o.o.")
      DistributionCompany.create({product_id: p4.id, company_id: cd4.id}, without_protection: true)
      cm4 = Company.find_by(name: "Soko Štark d.o.o.")
      ManufactureCompany.create({product_id: p4.id, company_id: cm4.id}, without_protection: true)

      p5 = Product.create({name: "Dobro jutro - mlečni margarin", barcode: "8600498172009", description: "mlečni margarin", net_weight: "250", reference_unit: "g", origin: "Srbija"}, without_protection: true)
      cd5 = Company.find_by(name: "Put-Gross d.o.o.")
      DistributionCompany.create({product_id: p3.id, company_id: cd5.id}, without_protection: true)
      cm5 = Company.find_by(name: "Dijamant a.d.")
      ManufactureCompany.create({product_id: p5.id, company_id: cm5.id}, without_protection: true)
      p5i1 = Ingredient.find_by(name: "biljno ulje")
      p5i2 = Ingredient.find_by(name: "biljna mast")
      p5i3 = Ingredient.find_by(name: "voda")
      p5i4 = Ingredient.find_by(name: "so")
      p5i5 = Ingredient.find_by(name: "obrano mlijeko u prahu")
      p5i6 = Ingredient.find_by(name: "mliječna kiselina")
      p5i7 = Ingredient.find_by(name: "beta karoten")
      p5i8 = Ingredient.find_by(name: "monogliceridi i digliceridi masnih kiselina")
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i1.id}, without_protection: true)
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i2.id}, without_protection: true)
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i3.id}, without_protection: true)
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i4.id}, without_protection: true)
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i5.id}, without_protection: true)
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i6.id}, without_protection: true)
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i7.id}, without_protection: true)
      ProductIngredient.create({product_id: p5.id, ingredient_id: p5i8.id}, without_protection: true)
      p5v1 = Vitamin.find_by(name: "Vitamin A")
      p5v2 = Vitamin.find_by(name: "Vitamin E")
      p5v3 = Vitamin.find_by(name: "Vitamin D")
      ProductVitamin.create({product_id: p5.id, vitamin_id: p5v1.id, amount: 20}, without_protection: true)
      ProductVitamin.create({product_id: p5.id, vitamin_id: p5v2.id, amount: 1650}, without_protection: true)
      ProductVitamin.create({product_id: p5.id, vitamin_id: p5v3.id, amount: 7.5}, without_protection: true)
      p5n1 = Nutrient.find_by(name: "Energetska vrijednost")
      p5n2 = Nutrient.find_by(name: "Proteini")
      p5n3 = Nutrient.find_by(name: "Ugljeni hidrati")
      p5n4 = Nutrient.find_by(name: "Masti")
      p5n5 = Nutrient.find_by(name: "Zasićene masti")
      p5n6 = Nutrient.find_by(name: "Jednostruko nezasićene masne kisjeline")
      p5n7 = Nutrient.find_by(name: "Višestruko nezasićene masne kisjeline")
      ProductNutrient.create({product_id: p5.id, nutrient_id: p5n1.id, amount: 2251}, without_protection: true)
      ProductNutrient.create({product_id: p5.id, nutrient_id: p5n2.id, amount: 0.51}, without_protection: true)
      ProductNutrient.create({product_id: p5.id, nutrient_id: p5n3.id, amount: 0.54}, without_protection: true)
      ProductNutrient.create({product_id: p5.id, nutrient_id: p5n4.id, amount: 60}, without_protection: true)
      ProductNutrient.create({product_id: p5.id, nutrient_id: p5n5.id, amount: 19}, without_protection: true)
      ProductNutrient.create({product_id: p5.id, nutrient_id: p5n6.id, amount: 16}, without_protection: true)
      ProductNutrient.create({product_id: p5.id, nutrient_id: p5n7.id, amount: 25}, without_protection: true)
      p5m1 = Mineral.find_by(name: "Cink")
      p5m2 = Mineral.find_by(name: "Bakar")
      p5m3 = Mineral.find_by(name: "Hrom")
      ProductMineral.create({product_id: p5.id, mineral_id: p5m1.id, amount: 800}, without_protection: true)
      ProductMineral.create({product_id: p5.id, mineral_id: p5m2.id, amount: 12}, without_protection: true)
      ProductMineral.create({product_id: p5.id, mineral_id: p5m3.id, amount: 5}, without_protection: true)
   end

   task populate_brands: :environment do
      Brand.destroy_all
      cd1 = Company.find_by(name: "Klikovac d.o.o.")
      b1 =  Brand.create({name: "Najljepše želje", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/najljepse_zelje.png"), imageable: b1}, without_protection: true)
      b2 = Brand.create({name: "Coca-Cola", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/coca_cola.png"), imageable: b2}, without_protection: true)
      b3 = Brand.create({name: "Čipsi", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/chipsy.png"), imageable: b3}, without_protection: true)
      b4 = Brand.create({name: "Smoki", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/smoki.png"), imageable: b4}, without_protection: true)
      b5 = Brand.create({name: "Orbit", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/orbit.png"), imageable: b5}, without_protection: true)
      b6 = Brand.create({name: "Milka", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/milka.png"), imageable: b6}, without_protection: true)
      b7 = Brand.create({name: "Kinder", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/kinder.png"), imageable: b7}, without_protection: true)
      b8 = Brand.create({name: "Pardon", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/pardon.png"), imageable: b8}, without_protection: true)
      b9 = Brand.create({name: "Prima", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/prima.png"), imageable: b9}, without_protection: true)
      b10 = Brand.create({name: "Još", company_id: cd1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/jos.png"), imageable: b10}, without_protection: true)

      cm1 = Company.find_by(name: "Bambi a.d.")
      b11 = Brand.create({name: "Podravka", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/podravka.png"), imageable: b11}, without_protection: true)
      b12 = Brand.create({name: "Jaffa", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/jaffa.png"), imageable: b12}, without_protection: true)
      b13 = Brand.create({name: "Medeno srce", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/medeno_srce.png"), imageable: b13}, without_protection: true)
      b14 = Brand.create({name: "Bonžita", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/bonzita.png"), imageable: b14}, without_protection: true)
      b15 = Brand.create({name: "Dorina", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/dorina.png"), imageable: b15}, without_protection: true)
      b16 = Brand.create({name: "Wellness", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/wellness.png"), imageable: b16}, without_protection: true)
      b17 = Brand.create({name: "Integrino", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/integrino.png"), imageable: b17}, without_protection: true)
      b18 = Brand.create({name: "Rafaelo", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/rafaelo.png"), imageable: b18}, without_protection: true)
      b19 = Brand.create({name: "Mars", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/mars.png"), imageable: b19}, without_protection: true)
      b20 = Brand.create({name: "Twix", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/twix.png"), imageable: b20}, without_protection: true)
      b21 = Brand.create({name: "Snickers", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/snickers.png"), imageable: b21}, without_protection: true)
      b22 = Brand.create({name: "Cupa Chups", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/chupa_chups.png"), imageable: b22}, without_protection: true)
      b23 = Brand.create({name: "Red Bull", company_id: cm1.id}, without_protection: true)
      Photo.create({image: File.open("public/images/logos/red_bull.png"), imageable: b23}, without_protection: true)
    end
  end

  namespace :db do
    task :populate => [
                        :populate_companies,
                        :populate_minerals, 
                        :populate_nutrients, 
                        :populate_vitamins, 
                        :populate_tags, 
                        :populate_symbols,
                        :populate_ingredients,
                        :populate_certificates,
                        :populate_products,
                        :populate_brands,
                        :populate_stores,
                        :populate_locations
                      ]
  end
end